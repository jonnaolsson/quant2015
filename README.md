# Quantitative Macroeconomics Methods#
Jonna Olsson

All code can be found in model/src/. Click on "Source" to the left to go to the file structure. 

## Part 1 (Kurt) ##
If you want to run the models, download the repository and create the subfolders results, plots/huggett and plots/ks. 
### 1. Huggett ###
* The code used for the Huggett model can be found in the subfolder [huggett](https://bitbucket.org/jonnaolsson/quant2015/src/3337210f89691b022b487c82266ea23e54e342b5/model/src/huggett/?at=master). 
* ```main.py``` Running this solves first the autarky model, then the fiat money equilibrium, and finally the two transition paths. It saves all output to a file that will be placed in the results subfolder. 
* ```plot_res.py``` This analyzes the results and plots all graphs, which will be placed in the plots/huggett folder. 
* ```params.py``` Here all the parameters for the run are given. 

### 2. KS ###
* The code used for the KS model can be found in the subfolder [ks](https://bitbucket.org/jonnaolsson/quant2015/src/3337210f89691b022b487c82266ea23e54e342b5/model/src/ks/?at=master)
* ```main.py``` Running this first solves the Aiyagari model for the three different TFP shock levels, then solves the KS model (using results from the Aiyagari model as input). Results are placed in the results subfolder
* ```plot_res.py``` This analyzes the results and plots all graphs. 
* ```params.py``` Here all the parameters for the run are given, including which exercise you are solving (economy 1, 2 or 3).

## Part 2 (Kathrin) ##
If you want to run the models, download the repository and create the subfolders results and plots/lc. 

* The code used for all exercises can be found in the subfolder model/src/lifecycle
* ```params.py``` Here all the parameters for the run are given, including which exercise you are solving (1, 3, 4 or 5). 
* To be able to solve exercise 5, you must have solved exercise 1 first (since it will reuse those value functions and policy functions)
* ```main.py``` Program to start run - even though it does not do much, just checks which exercise you are solving, and sends you to the right function.
* ```ex134.py``` Core program for solving exercise 1, 3 and 4.
* ```ex5.py``` Core program for solving exercise 5.
* ```solve_egm.py```, ```solve_pfi.py``` and ```solve_vfi.py``` Contain the different solving algorithms. Note that the vfi file includes two separate functions depending on if you are in ex 1-4 or in ex 5. 
* ```simulate.py``` Contains the main simulation logics. Here as well the logic for exercise 5 is separated. 
* Results from the solving and the simulations are stored in the results subfolder. 
* Plotting of results are made in ```plot_res_ex1.py```, ```plot_res_ex34.py``` and ```plot_res_ex5.py```