from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from ks.envir import resdir
import pickle
import numpy as np
from ks.params import par
from ks.initialize import initialize, initialize_aiy
from ks.results import Results
import matplotlib.pyplot as plt
from time import perf_counter
from numba import jit


def aiyagari_kernel(a, v0, g0, omega0):
    vfun_res = []
    gfun_res = []
    omega_res = []
    r_res = []
    k_agg_res = []

    for it in range(par.agg_tech.values.shape[0]):
        agg_tech = par.agg_tech.values[it]
        msg = '\n**** Solving Aiyagari, TFP = {:.2f} ****'
        print(msg.format(agg_tech))
        r_lb = 0.0
        r_ub = 0.12
        t_start = perf_counter()
        r, omega, vfun, gfun = aiyagari(a, agg_tech, v0, g0, omega0, r_lb, r_ub)
        # Compute implied aggregate capital stock
        k_agg = np.sum(omega * a)
        msg = 'Aggregate capital stock in equilibrium: {:.4f} (TFP: {:.2f})'
        print(msg.format(k_agg, agg_tech))

        vfun_res.append(vfun)
        gfun_res.append(gfun)
        omega_res.append(omega)
        r_res.append(r)
        k_agg_res.append(k_agg)

        t_stop = perf_counter()
        msg = 'Aiyagari solved in {:.2f} sec\n\n'
        print(msg.format(t_stop - t_start))

    return vfun_res, gfun_res, omega_res, r_res, k_agg_res


def vfi_aiy(a, v0, g0, r, w, maxiter=500, tol=1e-8):
    tm_m = par.labor.tm
    labor = par.labor.values
    tau = par.taxes.tau
    benefits = par.taxes.benefits
    beta = par.beta
    util = par.util
    anum = a.shape[0]
    lnum = par.labor.values.shape[0]
    eps = 1

    # Initialize value functions
    v_n = np.array(v0, copy=True)
    v_n1 = np.empty_like(v_n)

    # Initialize policy function
    g = np.array(g0, copy=True)

    # Precompute resources available at each gridpoint
    income = w*labor*(1-tau) + w*benefits
    resources = income.reshape((-1, 1)) + (1+r)*a.reshape((1, -1))

    for it in range(maxiter):
        for il_from in range(lnum):
            ap = 0
            tm = tm_m[il_from]
            exp_v = np.dot(tm, v_n)
            if par.sigma == 1:
                v_n1, g = optimize_log(a, anum, ap, resources, il_from, exp_v,
                                       beta, v_n1, g)
            else:
                v_n1, g = optimize_nonlog(a, anum, ap, resources, il_from,
                                          exp_v, beta, v_n1, g, par.sigma)

        # if it % 200 == 0:
        #     msg = 'VFI: iteration {:3d}, eps={:g}'
        #     print(msg.format(it, eps))

        eps = np.max(np.abs(v_n-v_n1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break

        v_n, v_n1 = v_n1, v_n
    else:
        msg = 'No convergence in iterate_het, eps={:g}'
        raise Exception(msg.format(eps))

    return v_n1, g


@jit
def optimize_log(a, anum, ap, resources, il_from, exp_v, beta, v_n1, g):
    for ia_from in range(anum):
        vmax = -np.inf
        res_a = resources[il_from, ia_from]
        for ap in range(ap, anum):
            a_to = a[ap]
            if a_to > res_a:
                break
            v = np.log(res_a-a_to) + beta*exp_v[ap]
            if v < vmax:
                ap -= 1
                break
            vmax = v
        v_n1[il_from, ia_from] = vmax
        g[il_from, ia_from] = ap
    return v_n1, g


@jit
def optimize_nonlog(a, anum, ap, resources, il_from, exp_v, beta, v_n1, g,
                    sigma):
    for ia_from in range(anum):
        vmax = -np.inf
        res_a = resources[il_from, ia_from]
        for ap in range(ap, anum):
            a_to = a[ap]
            if a_to > res_a:
                break
            v = (np.power(res_a - a_to, 1-sigma)-1) / (1-sigma) + \
                beta*(exp_v[ap])
            if v < vmax:
                ap -= 1
                break
            vmax = v
        v_n1[il_from, ia_from] = vmax
        g[il_from, ia_from] = ap
    return v_n1, g


def aiyagari(a, agg_tech, v0, g0, omega0, r_lb, r_ub, maxiter=500, tol=1e-4):
    alpha = par.alpha

    for it in range(maxiter):
        msg = '\nMain iteration {:.0f} started... '
        print(msg.format(it))
        # Calculate new guess for r
        r = (r_lb + r_ub)/2
        msg = 'New r: {:.5f}'
        print(msg.format(r))

        # Compute capital and wage
        k = par.labor.lbar * (r/(alpha*agg_tech))**(1/(alpha-1))
        w = agg_tech * (1-alpha)*k**alpha * par.labor.lbar**(-alpha)

        # Solve HHP given prices
        vfun, gfun = vfi_aiy(a, v0, g0, r, w)

        # Solve for invariant distribution
        omega = comp_dist(omega0, a, gfun)

        # Compute aggregate capital implied by HHP
        k_new = np.sum(omega * a)
        msg = 'Aggregate capital: {:.3f}'
        print(msg.format(k_new))

        # Compute implied interest rate
        r_new = alpha * agg_tech * (k_new / par.labor.lbar)**(alpha-1)
        msg = 'Implied interest rate r: {:.4f}'
        print(msg.format(r_new))

        eps = r-r_new
        msg = 'r: {:.5f}, r_new: {:.5f}, eps: {:.4f}'
        print(msg.format(r, r_new, eps))
        if np.abs(eps) < tol:
            break

        # If continue, update guess for p
        if eps > 0:
            r_ub = r
        else:
            r_lb = r

    return r, omega, vfun, gfun


def comp_dist(omega0, a_grid, g, maxiter=20000, tol=1e-6):
    eps = 1
    tm_m = par.labor.tm
    anum = a_grid.shape[0]
    lnum = par.labor.values.shape[0]

    omega0 = np.array(omega0, copy=True)
    omega = np.zeros((lnum, anum))

    for it in range(maxiter):
        omega[:] = 0.0
        omega = comp_new_dist(anum, lnum, g, omega0, omega, tm_m)
        eps = np.max(np.abs(omega-omega0))
        if eps < (tol*np.max(omega)):
            msg = 'Comp dist iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break
        omega, omega0 = omega0, omega

    else:
        msg = 'No convergence in comp_dist, eps={:g}'
        raise Exception(msg.format(eps))

    return omega


@jit
def comp_new_dist(anum, lnum, g, omega0, omega, tm_m):
    for l in range(lnum):
        for a in range(anum):
            ap = g[l, a]
            for lp in range(lnum):
                omega[lp, ap] = omega[lp, ap] + tm_m[l, lp]*omega0[l, a]
    return omega


def main():
    print('Hello world - Baseline Aiyagari model is running! \n')
    a, k, coeff, omega0, v0, g0 = initialize(par)
    omega0, v0, g0 = initialize_aiy(par)

    vfun_res, gfun_res, omega_res, r_res, k_agg_res = aiyagari_kernel(a, v0, g0,
                                                                      omega0)

    # Store the results
    res_aiy = Results()
    res_aiy.vfun_aiy = vfun_res
    res_aiy.gfun_aiy = gfun_res
    res_aiy.omega_aiy = omega_res
    res_aiy.r_aiy = r_res
    res_aiy.k_agg_aiy = k_agg_res

    res_aiy.a = a
    res_aiy.k = k
    # Remove the util function since you cannot pickle lambda functions
    par.util = None
    res_aiy.par = par
    # Pickle and save for future
    filen = 'res_ks_aiy_sigma%.d.bin' % res_aiy.par.sigma
    fn = join(resdir, filen)
    pickle.dump(res_aiy, open(fn, 'wb'))
    print('good bye world')


if __name__ == '__main__':
    main()
