from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from ks.envir import resdir, plotdir
from ks.analyze import calc_ineq, calc_quintiles, check_agg_cap, average_quintiles
from common.plotting import plot_gen, plot_lorenz, get_filename
import pickle
import numpy as np
import matplotlib.pyplot as plt


def load_data():
    fn = join(resdir, 'res_ks_start_sigma1_ex3.bin')
    res = pickle.load(open(fn, 'rb'))

    a = res.a

    return res, a


def fix_colors():
    colcyc = ['#ff7f00', '#377eb8', '#4daf4a', '#984ea3', '#e41a1c']
    return colcyc


def plot_aiy(res, a):
    colcyc = fix_colors()

    for it in range(len(res.vfun_aiy)):
        # Create grid of actual asset choices
        vfun = res.vfun_aiy[it]
        gfun = res.gfun_aiy[it]
        anext = a[gfun]

        title = 'Value function Aiyagari E%.d (TFP: %.d)' \
                % (res.par.exercise, it)
        text = 'r: %.6f, K: %.4f' % (res.r_aiy[it], res.k_agg_aiy[it])
        fn = get_filename(res, 'Aiy', 'vfun', tfp=it)
        plot_gen(a, vfun,
                 mult_lin=True,
                 title=title,
                 xlab='Assets', ylab='Value',
                 xlim=[np.min(a), np.max(a)],
                 ylim=[np.min(vfun), np.max(vfun)*1.05],
                 fname=join(plotdir, fn),
                 cols=colcyc,
                 text=text,
                 llabs=res.par.labor.values)

        title = 'Policy function Aiyagari E%.d (TFP: %.d)' \
                % (res.par.exercise, it)
        fn = get_filename(res, 'Aiy', 'pfun', tfp=it)
        plot_gen(a, anext,
                 mult_lin=True,
                 title=title,
                 xlab='Assets', ylab='Next period assets',
                 xlim=[np.min(a), np.max(a)],
                 ylim=[np.min(anext), np.max(anext)*1.05],
                 fname=join(plotdir, fn),
                 cols=colcyc,
                 text=text,
                 llabs=res.par.labor.values)

        a_dist = np.sum(res.omega_aiy[it], axis=0)
        title = 'Distribution Aiyagari E%.d (TFP: %.d)' % \
                (res.par.exercise, it)
        fn = get_filename(res, 'Aiy', 'dist', tfp=it)
        plot_gen(a, a_dist,
                 mult_lin=False,
                 title=title,
                 xlab='Assets', ylab='Distribution',
                 xlim=[np.min(a), np.max(a)*0.5],
                 #ylim=[np.min(a_dist), np.max(a_dist)*1.05],
                 ylim=[0, 0.01],
                 fname=join(plotdir, fn),
                 cols=colcyc,
                 text=text)


def main():
    print('Start plot_res')
    res, a = load_data()
    colors = fix_colors()

    msg = 'Number of grid points {:3d}'
    print(msg.format(a.shape[0]))

    plot_aiy(res, a)
    omega = res.omega[-1]
    labor = res.par.labor.values
    calc_ineq(omega, labor, a, colors, res.par.sigma, res.par.exercise, res)
    quint_stat = res.quint_stat[res.par.simulation.burn_in:]
    average_quintiles(quint_stat)
    quint_stat2 = calc_quintiles(omega, a, labor)
    check_agg_cap(res, quint_stat2)

    msg = '\nCoefficients: (sigma=%.d, exercise %.d)' % (
        res.par.sigma, res.par.exercise)
    print(msg)
    print(res.coeff[len(res.coeff)-1])

    msg = '\nEquilibrium tau: %.3f percent' % (res.par.taxes.tau*100)
    print(msg)

    print('\nFinished plot_res')


if __name__ == '__main__':
    main()
