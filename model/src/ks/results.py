from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'


class Results:
    def __init__(self):
        self.par = None

        self.vfun = None
        self.gfun = None
        self.omega = None
        self.quint_stat = None
        self.coeff = None
        self.rsquared = None

        self.vfun_aiy = None
        self.gfun_aiy = None
        self.r_aiy = None
        self.k_agg_aiy = None

        self.a = None
        self.k = None

