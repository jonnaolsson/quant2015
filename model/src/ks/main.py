from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from ks.envir import resdir
import pickle
import numpy as np
from ks.params import par
from ks.initialize import initialize, initialize_aiy, initialize_kgrid
from ks.results import Results
from ks.vfi import vfi
from ks.simulation import get_new_coeff
from ks.aiyagari import aiyagari_kernel
import matplotlib.pyplot as plt
from time import perf_counter


def outer_loop(a, k, coeff, v0, g0, omega0, i_agg_level, maxiter=500, tol=1e-6):
    vfun_it = []
    gfun_it = []
    omega_it = []
    coeff_it = []
    rsquared_it = []
    eps = 1
    t_startouter = perf_counter()
    for it in range(maxiter):
        msg = '\nIteration {:.0f} starting... '
        print(msg.format(it))

        print('Start solving the HHP using VFI...')
        t1 = perf_counter()
        gfun, vfun = vfi(a, k, coeff, v0, g0, tol=1e-7)
        t2 = perf_counter()
        msg = 'VFI done in {:.2f} sec'
        print(msg.format(t2-t1))

        coeff_new, rsquared, nobs, omega, quint = get_new_coeff(a, k, gfun, omega0, coeff,
                                                   i_agg_level, quint=False)
        eps = np.max(np.abs(coeff_new - coeff))
        print('Old coefficients: ')
        print(coeff)
        print('New coefficients: ')
        print(coeff_new)
        print('R2:')
        print(rsquared)
        print('nobs:')
        print(nobs)
        t_current = perf_counter()
        msg = 'Iteration {:.0f} done, eps={:.6f}, time since start: {:.2f} min'
        print(msg.format(it, eps, (t_current-t_startouter)/60))
        vfun_it.append(vfun)
        gfun_it.append(gfun)
        omega_it.append(omega)
        coeff_it.append(coeff_new)
        rsquared_it.append(rsquared)
        if eps < tol:
            print('wow, it converged, running last simulation to get c, income, etc.')
            coeff_new, rsquared, nobs, omega, quint_stat = \
                get_new_coeff(a, k, gfun, omega0, coeff, i_agg_level,
                              quint=True, pfun=gfun)
            vfun_it.append(vfun)
            gfun_it.append(gfun)
            omega_it.append(omega)
            coeff_it.append(coeff_new)
            rsquared_it.append(rsquared)

            break

        coeff_next = update_coeff(coeff, coeff_new)
        coeff = coeff_next
        v0 = vfun

    return vfun_it, gfun_it, omega_it, coeff_it, rsquared_it, quint_stat


def update_coeff(coeff, coeff_new):
    gamma = 0.5
    coeff_next = coeff*(1-gamma) + coeff_new*gamma
    return coeff_next


def main():
    t_start = perf_counter()
    print('Hello world - KS model is running! \n')
    msg = 'Aggregate shocks: {:.3f}, {:.3f}, {:.3f}'
    print(msg.format(par.agg_tech.values[0], par.agg_tech.values[1],
                     par.agg_tech.values[2]))
    a, coeff, omega0, v0, g0 = initialize(par)
    res = Results()

    # First solve Aiygari model
    omega0, v0, g0 = initialize_aiy(par)
    vfun_res, gfun_res, omega_res, r_res, k_agg_res \
        = aiyagari_kernel(a, v0, g0, omega0)
    # Store results from Aiyagari
    res.vfun_aiy = vfun_res
    res.gfun_aiy = gfun_res
    res.omega_aiy = omega_res
    res.r_aiy = r_res
    res.k_agg_aiy = k_agg_res

    # Outer loop for KS
    print('\n**** Solving KS model ****\n')
    kmin = k_agg_res[0]
    kmax = k_agg_res[2]
    a, coeff, omega0, v0, g0 = initialize(par)
    k = initialize_kgrid(par, kmin, kmax)
    print('K grid is the following: ')
    print(k)

    i_agg_level = 2
    omega0 = omega_res[i_agg_level]
    vfun_it, gfun_it, omega_it, coeff_it, rsquared_it, quint_stat \
        = outer_loop(a, k, coeff, v0, g0, omega0, i_agg_level)

    # Store the results
    res.vfun = vfun_it
    res.gfun = gfun_it
    res.omega = omega_it
    res.quint_stat = quint_stat
    res.coeff = coeff_it
    res.rsquared = rsquared_it
    res.a = a
    res.k = k
    # Remove the util function since you cannot pickle lambda functions
    par.util = None
    res.par = par
    # Pickle and save for future
    filen = 'res_ks_start_sigma%.d_ex%.d.bin' % (res.par.sigma,
                                                 res.par.exercise)
    fn = join(resdir, filen)
    pickle.dump(res, open(fn, 'wb'))
    t_end = perf_counter()
    msg = 'good bye world, after {:.2f} min...'
    print(msg.format((t_end-t_start)/60))


if __name__ == '__main__':
    main()