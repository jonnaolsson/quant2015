from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np


def initialize(par):
    grid_a = par.grid_a
    grid_k = par.grid_k
    tm_m = par.labor.tm
    labor = par.labor.values
    benefits = par.taxes.benefits

    # Calculate invariate labor distribution l_inv
    l_invar = np.linalg.matrix_power(tm_m, 999)
    l_inv = l_invar[0]
    par.labor.l_inv = l_inv

    # Create grid for assets
    correction = 10**grid_a.logmin
    a = np.logspace(grid_a.logmin, grid_a.logmax, grid_a.n) - correction

    # Create grid for aggregate capital
    k = np.linspace(grid_k.min, grid_k.max, grid_k.n)

    # Initialize guess for lom capital coefficients
    coeff = np.array([[0.0, 0.0, 0.0],
                      [1.0, 1.0, 1.0]])

    # Calculate total labor supply
    lbar = np.dot(labor, l_inv)
    par.labor.lbar = lbar
    # Calculate correct tax amount
    ben_tot = np.dot(benefits, l_inv)
    par.taxes.tau = ben_tot / lbar

    # Get the dimensions right
    aggnum = par.agg_tech.values.shape[0]
    knum = k.shape[0]
    anum = a.shape[0]
    lnum = labor.shape[0]

    # Initialize vfun and pfun
    v0 = np.zeros((aggnum, knum, lnum, anum))
    g0 = np.zeros((aggnum, knum, lnum, anum), dtype=np.uint32)

    # Initialize Omega
    omega0 = np.ones((lnum, anum))/(lnum*anum)

    return a, coeff, omega0, v0, g0


def initialize_kgrid(par, kmin, kmax):
    # Create grid for aggregate capital
    grid_k = par.grid_k
    k = np.linspace(kmin*0.8, kmax*1.2, grid_k.n)
    return k


def initialize_aiy(par):

    # Get the dimensions right
    anum = par.grid_a.n
    lnum = par.labor.values.shape[0]

    # Initialize vfun and pfun
    v0 = np.zeros((lnum, anum))
    g0 = np.zeros((lnum, anum), dtype=np.uint32)

    # Initialize Omega
    omega0 = np.ones((lnum, anum))/(lnum*anum)

    return omega0, v0, g0