from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
import numpy as np
import pickle
import statsmodels.api as sm
from time import perf_counter
from ks.initialize import initialize
from ks.envir import resdir
from ks.params import par
from numba import jit


def load_data():
    fn = join(resdir, 'res_ks_start_sigma2.bin')
    res = pickle.load(open(fn, 'rb'))
    return res


@jit
def update_omega_n(lnum, anum, gfun, agg_state, k_low, k_high, tm_m, omega0,
                   omega_low, omega_high):
    for i_lfrom in range(lnum):
        for i_afrom in range(anum):
            i_ato_low = gfun[agg_state, k_low, i_lfrom, i_afrom]
            i_ato_high = gfun[agg_state, k_high, i_lfrom, i_afrom]
            for i_lto in range(lnum):
                omega_low[i_lto, i_ato_low] += \
                    tm_m[i_lfrom, i_lto]*omega0[i_lfrom, i_afrom]
                omega_high[i_lto, i_ato_high] += \
                    tm_m[i_lfrom, i_lto]*omega0[i_lfrom, i_afrom]
    return omega_low, omega_high


def update_dist(a, k, agg_state, k_agg, omega0, gfun):
    tm_m = par.labor.tm
    anum = a.shape[0]
    lnum = par.labor.values.shape[0]
    omega_high = np.zeros((lnum, anum))
    omega_low = np.zeros((lnum, anum))

    gamma = np.interp(k_agg, k, np.arange(k.shape[0]))
    k_low = np.floor(gamma)
    k_high = np.ceil(gamma)
    gamma = gamma - k_low
    omega_low, omega_high = update_omega_n(lnum, anum, gfun, agg_state, k_low,
                                           k_high, tm_m, omega0, omega_low,
                                           omega_high)
    omega = omega_low*(1-gamma) + omega_high*gamma

    check = np.sum(omega)
    if np.abs(check-1) > 1e-6:
        msg = 'Something wrong with new omega, sum = {:.2f}'
        print(msg.format(check))

    return omega


# Function to calculate quintile statistics - only run in the last
# simulation round
def calc_quint_gen(omega, a, k, k_agg, i_agg_level, pfun):
    labor = par.labor.values
    benefits = par.taxes.benefits
    tau = par.taxes.tau
    alpha = par.alpha
    agg_tech = par.agg_tech.values

    r = alpha * agg_tech[i_agg_level] * \
                    (k_agg / par.labor.lbar)**(alpha-1)
    w = (1-alpha) * agg_tech[i_agg_level] * \
                    (k_agg / par.labor.lbar)**alpha

    quintiles = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
    q_indices = []
    people_cum = np.cumsum(np.sum(omega, axis=0))
    asset_omega = omega * a

    inc_omega = np.transpose(omega) * (w*labor*(1-tau) + benefits)
    inc_omega = np.transpose(inc_omega)
    inc_omega += asset_omega*r

    gamma = np.interp(k_agg, k, np.arange(k.shape[0]))
    k_low = np.floor(gamma)
    k_high = np.ceil(gamma)
    gamma = gamma - k_low

    savings_matrix = ((1-gamma)*a[pfun[i_agg_level, k_low, :, :]] +
                      gamma*a[pfun[i_agg_level, k_high, :, :]])
    sav_omega = omega * savings_matrix
    cons_omega = inc_omega + asset_omega - sav_omega

    omega_per_q = []
    asset_omega_per_q = []
    inc_omega_per_q = []
    sav_omega_per_q = []
    cons_omega_per_q = []

    for q in quintiles:
        q_indices.append(np.argmin(np.abs(people_cum - q)))

    for it in range(len(q_indices)):
        if it == 0:
            pass
        omega_per_q.append(omega[:, q_indices[it-1]:q_indices[it]])
        asset_omega_per_q.append(asset_omega[:, q_indices[it-1]:q_indices[it]])
        inc_omega_per_q.append(inc_omega[:, q_indices[it-1]:q_indices[it]])
        sav_omega_per_q.append(sav_omega[:, q_indices[it-1]:q_indices[it]])
        cons_omega_per_q.append(cons_omega[:, q_indices[it-1]:q_indices[it]])

    quint_stat = np.empty((5, len(q_indices)))
    for it in range(len(q_indices)):
        quint_stat[0, it] = np.sum(omega_per_q[it])
        quint_stat[1, it] = np.sum(asset_omega_per_q[it])
        quint_stat[2, it] = np.sum(inc_omega_per_q[it])
        quint_stat[3, it] = np.sum(sav_omega_per_q[it])
        quint_stat[4, it] = np.sum(cons_omega_per_q[it])


    # Just for now: trim it so that it is easier to read:
    quint_stat = quint_stat[:, 1:]
    #print('\nPeople, assets, income:')
    #print(quint_stat)

    return quint_stat


def update_agg(i_agg_0, draw):
    tm = par.agg_tech.tm[i_agg_0]

    if draw < tm[0]:
        i_agg_next = 0
    elif draw < (tm[0] + tm[1]):
        i_agg_next = 1
    else:
        i_agg_next = 2

    return i_agg_next


def simulate(a, k, gfun, omega0, i_agg_level, quint, pfun):
    print('Starting simulations... ')
    t1 = perf_counter()
    simulations = par.simulation.n
    burn_in = par.simulation.burn_in
    sim_res_a = np.zeros(simulations, dtype=np.uint32)
    sim_res_k = np.zeros(simulations)
    sim_res_omega = []
    sim_res_quint_stat = []

    # Create "random" draw of TFP shocks
    np.random.seed(4242)
    rand_agg = np.random.rand(par.simulation.n)
    # Initialize aggregate TFP and k for first period
    k_agg = np.sum(omega0 * a)

    for it in range(simulations):
        sim_res_a[it] = i_agg_level
        sim_res_k[it] = k_agg
        omega = update_dist(a, k, i_agg_level, k_agg, omega0, gfun)

        if quint:
            sim_res_omega.append(omega)
            quint_stat = calc_quint_gen(omega, a, k, k_agg, i_agg_level, pfun)
            sim_res_quint_stat.append(quint_stat)

        k_prime = np.sum(omega * a)
        i_agg_level_next = update_agg(i_agg_level, rand_agg[it])

        # Update for next period
        k_agg = k_prime
        i_agg_level = i_agg_level_next
        omega0 = omega

        # if it % 4000 == 0:
        #     t_current = perf_counter()
        #     msg = 'Simulations done: {:3d} (since start: {:.2f} sec)'
        #     print(msg.format(it, (t_current-t1)))

    t2 = perf_counter()
    msg = '{:.0f} simulations in {:.2f} sec'
    print(msg.format(simulations, (t2-t1)))
    sim_res_a = sim_res_a[burn_in:]
    sim_res_k = sim_res_k[burn_in:]
    if quint:
        sim_res_omega = sim_res_omega[burn_in:]
        sim_res_omega = sum(sim_res_omega)/len(sim_res_omega)

    return sim_res_a, sim_res_k, sim_res_omega, sim_res_quint_stat


def regress(sim_res_a, sim_res_k, coeff):
    coeff_new = np.array(coeff, copy=True)
    rsquared = []
    nobs = []
    # Partition the observations
    for i in range(par.agg_tech.tm.shape[1]):
        idx = sim_res_a == i
        k_log = np.log(sim_res_k[idx])
        false = np.array([False])
        kprime_idx = np.concatenate([false, idx])

        # Take away last observation since we don't have a kprime for it
        if kprime_idx[kprime_idx.shape[0]-1]:
            k_log = k_log[:(k_log.shape[0]-1)]
            kprime_idx[kprime_idx.shape[0]-1] = False
        kprime_log = np.log(sim_res_k[kprime_idx])

        # only regress if we have more than one observation
        if np.sum(kprime_idx) > 1:
            k_log = sm.add_constant(k_log)
            results = sm.OLS(kprime_log, k_log).fit()
            # print(results.summary())
            coeff_new[:, i] = results.params
            rsquared.append(results.rsquared)
            nobs.append(results.nobs)
    return coeff_new, rsquared, nobs


def get_new_coeff(a, k, gfun, omega0, coeff, i_agg_level, quint, pfun=None):
    sim_res_a, sim_res_k, omega, sim_res_quint_stat = \
        simulate(a, k, gfun, omega0, i_agg_level, quint, pfun)
    # print('TFP shocks: ')
    # print(sim_res_a)
    coeff_new, rsquared, nobs = regress(sim_res_a, sim_res_k, coeff)
    return coeff_new, rsquared, nobs, omega, sim_res_quint_stat


def main():
    a, k, coeff, omega0, v0, g0 = initialize(par)
    res = load_data()
    coeff_new, rsquared, omega = get_new_coeff(a, k, res.gfun, omega0, coeff)
    print(coeff_new)


if __name__ == '__main__':
    main()