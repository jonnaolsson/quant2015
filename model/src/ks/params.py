from __future__ import print_function, division, absolute_import

import numpy as np
from math import log as mlog


class ParamContainer:
    pass

par = ParamContainer()

# Runtime settings
par.tol = 1e-6
par.accel = False
cons_quint = False
par.exercise = 3

# Production parameters
par.alpha = 0.36
par.delta = 0

# Preference parameters
par.beta = 0.96
par.sigma = 1.0
if par.sigma == 1:
    par.util = lambda c: mlog(c)
else:
    par.util = lambda c: (np.power(c, 1-par.sigma)-1) / (1-par.sigma)

# Process for A
par.agg_tech = ParamContainer()
par.agg_tech.values = np.array((0.85, 1.0, 1.05))
par.agg_tech.tm = np.array([[0.800, 0.200, 0.000],
                            [0.025, 0.850, 0.125],
                            [0.000, 0.050, 0.950]])
assert np.all(np.abs(np.sum(par.agg_tech.tm, axis=1) - 1) < 1e-9)

# Labor endowment process
par.labor = ParamContainer()
if par.exercise == 3:
    par.labor.values = np.array((0.00, 0.75, 1.00, 1.25, 3.00))
else:
    par.labor.values = np.array((0.00, 0.75, 1.00, 1.25, 10.00))
par.labor.tm = np.array([[0.50, 0.20, 0.20, 0.10, 0.00],
                         [0.01, 0.80, 0.10, 0.09, 0.00],
                         [0.01, 0.04, 0.90, 0.05, 0.00],
                         [0.01, 0.00, 0.04, 0.90, 0.05],
                         [0.01, 0.09, 0.20, 0.20, 0.50]])
assert np.all(np.abs(np.sum(par.labor.tm, axis=1) - 1) < 1e-9)
# Initialize l_inv=0 and lbar=0, will be updated later
par.labor.l_inv = [0, 0, 0, 0, 0]
par.labor.lbar = 0

# Taxes and benefits
par.taxes = ParamContainer()
if par.exercise == 1:
    par.taxes.benefits = np.array((0.25, 0, 0, 0, 0))
else:
    par.taxes.benefits = np.array((0.7, 0, 0, 0, 0))
# Initialize tau=0, will be updated later to gov balance value
par.taxes.tau = 0

# Asset grid parameters
par.grid_a = ParamContainer()
par.grid_a.n = 6000
par.grid_a.logmin = -3
par.grid_a.logmax = 2.8

# Aggregate capital grid parameters
par.grid_k = ParamContainer()
par.grid_k.n = 10
par.grid_k.min = 5
par.grid_k.max = 80

# Simulation settings
par.simulation = ParamContainer()
par.simulation.n = 12000
par.simulation.burn_in = 2000
