from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'


from os.path import join
from ks.envir import resdir
import pickle
import numpy as np
from ks.params import par
from ks.initialize import initialize
from ks.results import Results
import matplotlib.pyplot as plt
from time import perf_counter
from numba import jit


def vfi(a, k, coeff, v0, g0, maxiter=500, tol=1e-3):
    t_vfistart = perf_counter()
    tm_l = par.labor.tm
    labor = par.labor.values
    benefits = par.taxes.benefits
    tau = par.taxes.tau
    alpha = par.alpha
    beta = par.beta
    util = par.util
    agg_tech = par.agg_tech.values
    aggnum = agg_tech.shape[0]
    knum = k.shape[0]
    anum = a.shape[0]
    lnum = par.labor.values.shape[0]

    accelerator = par.accel
    eps = 1

    # Initialize value functions
    v_n = np.array(v0, copy=True)
    v_n1 = np.empty_like(v_n)

    # Initialize policy function
    g = np.array(g0, copy=True)

    for it in range(maxiter):
        t_iterstart = perf_counter()

        for iagg_from in range(aggnum):
            tm_agg = par.agg_tech.tm[iagg_from]
            v_n_agg = np.zeros((knum, lnum, anum))
            for iagg_to in range(aggnum):
                v_n_agg += tm_agg[iagg_to]*v_n[iagg_to, :, :, :]

            for ik_from in range(knum):
                r = alpha * agg_tech[iagg_from] * \
                    (k[ik_from] / par.labor.lbar)**(alpha-1)
                w = (1-alpha) * agg_tech[iagg_from] * \
                    (k[ik_from] / par.labor.lbar)**alpha
                kp = np.exp(coeff[0, iagg_from] + coeff[1, iagg_from] *
                            np.log(k[ik_from]))
                gamma = np.interp(kp, k, np.arange(k.shape[0]))
                k_low = np.floor(gamma)
                k_high = np.ceil(gamma)
                gamma = gamma - k_low

                # Precompute resources available at each individual grid point
                income = w*labor*(1-tau) + w*benefits
                resources = income.reshape((-1, 1)) + (1+r)*a.reshape((1, -1))

                if not(it % 2 == 0) and accelerator:

                    for il_from in range(lnum):
                        tm = tm_l[il_from]
                        exp_v = np.zeros((knum, anum))
                        for il_to in range(lnum):
                            exp_v += tm[il_to] * v_n_agg[:, il_to, :]
                        for ia_from in range(anum):
                            ia_to = g[iagg_from, ik_from, il_from, ia_from]
                            a_to = a[ia_to]
                            c = resources[il_from, ia_from]-a_to
                            exp_v_ind = np.interp(kp, k, exp_v[:, ia_to])
                            v_n1[iagg_from, ik_from, il_from, ia_from] = \
                                util(c) + beta*exp_v_ind

                else:

                    for il_from in range(lnum):
                        ap = 0
                        tm = tm_l[il_from]
                        exp_v = np.zeros((knum, anum))
                        for il_to in range(lnum):
                            exp_v += tm[il_to] * v_n_agg[:, il_to, :]

                        if par.sigma == 1:
                            v_n1, g = optimize_log(a, anum, ap, resources,
                                                   il_from, exp_v, k_low,
                                                   k_high, beta, gamma,
                                                   iagg_from, ik_from, v_n1, g)
                        else:
                            v_n1, g = optimize_nonlog(a, anum, ap, resources,
                                                      il_from, exp_v, k_low,
                                                      k_high, beta, gamma,
                                                      iagg_from, ik_from, v_n1,
                                                      g, par.sigma)

        t_now = perf_counter()
        if it % 100 == 0:
            msg = 'VFI: it. {:3d}, eps={:4f}, time needed:{:.2f} sec (since ' \
                  'start: {:.1f} min)'
            print(msg.format(it, eps, (t_now - t_iterstart), ((t_now -
                  t_vfistart)/60)))

        eps = np.max(np.abs(v_n-v_n1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break
        v_n, v_n1 = v_n1, v_n
    else:
        msg = 'No convergence in iterate_het, eps={:g}'
        raise Exception(msg.format(eps))

    return g, v_n1


@jit
def optimize_log(a, anum, ap, resources, il_from, exp_v, k_low, k_high, beta,
             gamma, iagg_from, ik_from, v_n1, g):
    for ia_from in range(anum):
        vmax = -np.inf
        res_a = resources[il_from, ia_from]
        for ap in range(ap, anum):
            a_to = a[ap]
            if a_to > res_a:
                break
            # Interpolate over k values
            exp_v_ind_low = exp_v[k_low, ap]
            exp_v_ind_high = exp_v[k_high, ap]
            v = np.log(res_a - a_to) + beta*(exp_v_ind_low*(1-gamma) +
                                             exp_v_ind_high*gamma)
            if v < vmax:
                ap -= 1
                break
            vmax = v
        v_n1[iagg_from, ik_from, il_from, ia_from] = vmax
        g[iagg_from, ik_from, il_from, ia_from] = ap

    return v_n1, g


@jit
def optimize_nonlog(a, anum, ap, resources, il_from, exp_v, k_low, k_high, beta,
                    gamma, iagg_from, ik_from, v_n1, g, sigma):
    for ia_from in range(anum):
        vmax = -np.inf
        res_a = resources[il_from, ia_from]
        for ap in range(ap, anum):
            a_to = a[ap]
            if a_to > res_a:
                break
            # Interpolate over k values
            exp_v_ind_low = exp_v[k_low, ap]
            exp_v_ind_high = exp_v[k_high, ap]
            v = (np.power(res_a - a_to, 1-sigma)-1) / (1-sigma) + \
                beta*(exp_v_ind_low*(1-gamma) + exp_v_ind_high*gamma)
            if v < vmax:
                ap -= 1
                break
            vmax = v
        v_n1[iagg_from, ik_from, il_from, ia_from] = vmax
        g[iagg_from, ik_from, il_from, ia_from] = ap

    return v_n1, g