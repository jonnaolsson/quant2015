from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from common.plotting import plot_gen, plot_lorenz, get_filename, plot_lorenz3
from ks.envir import resdir, plotdir
import numpy as np


def calc_ineq(omega, labor, a, colcyc, sigma, exercise, res):
    a_pdf = np.sum(omega, axis=0)
    people_cum = np.cumsum(a_pdf)
    a_per_bracket = a * a_pdf
    a_cum = np.cumsum(a_per_bracket)

    # Calculating the Gini coefficient using wikipedia formula for
    # discrete pdf: G = 1 - (sum(f(y)(S_(i-1) + S_i))/(S_n)
    # where S_i = sum(f(y)y_i), i.e. a_cum
    numerator = 0.0
    for i in range(a.shape[0]):
        if i == 0:
            numerator += a_pdf[i] * (a_cum[i])
        else:
            numerator += a_pdf[i] * (a_cum[i - 1] + a_cum[i])
    gini_wealth = 1 - numerator / a_cum[-1]
    msg = '\nGini wealth: {:.3f}'
    print(msg.format(gini_wealth))

    # Find distribution of people per income class
    l_pdf = np.sum(omega, 1)
    l_people_cum = np.cumsum(l_pdf)

    # Note - PRE transfer and taxes!
    l_per_bracket = labor * l_pdf
    l_cum = np.cumsum(l_per_bracket)

    # Calculating the Gini for income (before taxes and transfers)
    numerator = 0.0
    for i in range(labor.shape[0]):
        if i == 0:
            numerator += l_pdf[i] * (l_cum[i])
        else:
            numerator += l_pdf[i] * (l_cum[i - 1] + l_cum[i])
    gini_income = 1 - numerator / l_cum[-1]
    msg = 'Gini earnings (pre): {:.3f}'
    print(msg.format(gini_income))

    # Note - POST transfer and taxes!
    tau = res.par.taxes.tau
    benefits = res.par.taxes.benefits
    l_post_per_bracket = (labor*(1-tau) + benefits) * l_pdf
    l_post_cum = np.concatenate(([0], np.cumsum(l_post_per_bracket)), axis=0)
    l_post_people_cum = np.concatenate(([0], l_people_cum), axis=0)
    l_post_pdf = np.concatenate(([0], l_pdf), axis=0)

    # Calculating the Gini for income (post taxes and transfers)
    numerator = 0.0
    for i in range(l_post_pdf.shape[0]):
        if i == 0:
            numerator += l_post_pdf[i] * (l_post_cum[i])
        else:
            numerator += l_post_pdf[i] * (l_post_cum[i - 1] + l_post_cum[i])
    gini_post_income = 1 - numerator / l_post_cum[-1]
    msg = 'Gini earnings (post): {:.3f}\n'
    print(msg.format(gini_post_income))

    # Plot Lorenz for wealth and income
    labels = ['Wealth', 'Earnings']
    text = 'Gini Wealth: %.3f \nGini Earnings: %.3f' \
           % (gini_wealth, gini_post_income)
    fn = get_filename(res, 'KS', 'Lorenz')
    #fn = 'KS_lorenz_sigma%.d_ex%.d.pdf' % (sigma, exercise)
    plot_lorenz(people_cum, a_cum/a_cum[-1],
                l_post_people_cum, l_post_cum/l_post_cum[-1],
                title='Lorenz curves E%.d' % exercise,
                xlab='Fraction of population',
                ylab='Cumulative share',
                fname=join(plotdir, fn),
                cols=colcyc,
                llabs=labels,
                text=text)


def calc_quintiles(omega, a, labor):
    quintiles = [0, 0.2, 0.4, 0.6, 0.8, 1.0]
    q_indices = []
    people_cum = np.cumsum(np.sum(omega, axis=0))
    asset_omega = omega * a
    # TODO: should it be with or without benefits? Add wages? Which wages?
    inc_omega = np.transpose(omega) * labor
    inc_omega = np.transpose(inc_omega)

    omega_per_q = []
    asset_omega_per_q = []
    inc_omega_per_q = []

    for q in quintiles:
        q_indices.append(np.argmin(np.abs(people_cum-q)))

    for it in range(len(q_indices)):
        if it == 0:
            pass
        asset_omega_per_q.append(asset_omega[:, q_indices[it-1]:q_indices[it]])
        omega_per_q.append(omega[:, q_indices[it-1]:q_indices[it]])
        inc_omega_per_q.append(inc_omega[:, q_indices[it-1]:q_indices[it]])
    
    quint_stat = np.empty((3, len(q_indices)))
    for it in range(len(q_indices)):
        quint_stat[0, it] = np.sum(omega_per_q[it])
        quint_stat[1, it] = np.sum(asset_omega_per_q[it])
        quint_stat[2, it] = np.sum(inc_omega_per_q[it])

    # Just for now: trim it so that it is easier to read:
    quint_stat = quint_stat[:, 1:]
    print('\nPeople, assets, income (pre taxes and transfers, not multiplied by w):')
    print(quint_stat)

    return quint_stat


def average_quintiles(quint_stat):
    quint_stat = sum(quint_stat)/len(quint_stat)
    print('People, wealth, income, savings, consumption: ')
    print(quint_stat)
    print('\nPer capita figures:')
    quint_stat /= 0.2
    print(quint_stat)


def check_agg_cap(res, quint_stat):
    a_inv = np.linalg.matrix_power(res.par.agg_tech.tm, 999)
    a_inv = a_inv[0]
    print('\nInvariate distribution of aggregate shocks: ')
    print(a_inv)
    cap_agg = np.sum(a_inv * res.k_agg_aiy)
    cap_agg_KS = np.sum(quint_stat, axis=1)[1]
    msg = '\nWeighted aiyagari cap stock: {:.2f}'
    print(msg.format(cap_agg))
    msg = 'Average KS cap stock: {:.2f}'
    print(msg.format(cap_agg_KS))
