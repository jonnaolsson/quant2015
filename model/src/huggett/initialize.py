from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np


def initialize(par):
    pg = par.grid_a
    tm_m = par.labor.tm
    labor = par.labor.values
    benefits = par.taxes.benefits

    # Calculate invariate labor distribution l_inv
    l_invar = np.linalg.matrix_power(tm_m, 999)
    l_inv = l_invar[0]
    par.labor.l_inv = l_inv

    # Create grid for assets
    correction = 10**pg.logmin
    a = np.logspace(pg.logmin, pg.logmax, pg.n) - correction

    # Calculate correct tax amount
    ben_tot = np.dot(benefits, l_inv)
    inc_tot = np.dot(labor, l_inv)
    par.taxes.tau = ben_tot / inc_tot

    # Get the dimensions right
    anum = a.shape[0]
    lnum = labor.shape[0]

    # Initialize vfun and pfun
    v0 = np.zeros((lnum, anum))
    g0 = np.zeros((lnum, anum), dtype=np.uint32)

    # Initialize Omega
    omega0 = np.ones((lnum, anum))/(lnum*anum)

    # Initialize omega_drop matrices
    omega_drop_a = calc_omega_drop(par, a, l_inv, 1, 1)
    omega_drop_b = calc_omega_drop(par, a, l_inv, 2, 1)

    return a, omega0, omega_drop_a, omega_drop_b, v0, g0


def calc_omega_drop(par, a, l_inv, method, price_level):
    # Get the dimensions right
    anum = a.shape[0]
    lnum = par.labor.values.shape[0]

    if method == 1:
        # If we drop on all equally
        omega_drop = np.zeros((lnum, anum))
        omega_drop[:, 0] = l_inv
        idx = np.argmin(abs(a - price_level))
        omega_drop[:, idx] = omega_drop[:, 0]
        omega_drop[:, 0] = 0

    if method == 2:
        # If poor gets it all
        omega_drop = np.zeros((lnum, anum))
        omega_drop[:, 0] = l_inv
        idx = np.argmin(abs(a - price_level/omega_drop[0, 0]))
        omega_drop[0, idx], omega_drop[0, 0] \
            = omega_drop[0, 0], omega_drop[0, idx]

    return omega_drop