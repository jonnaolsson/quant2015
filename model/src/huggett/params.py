from __future__ import print_function, division, absolute_import

# Dynamic programming problem parameters.
# Do not any environment-specific configuration to this file, use
# qmm_env_default.py for that instead!

import numpy as np
from math import log as mlog


class ParamContainer:
    pass

par = ParamContainer()

# Runtime settings
par.tol = 1e-6
par.accel = True

# Money supply parameters
par.m_tot = 1
par.drop_method = 1

# Transition path
par.trans_path = True
par.big_t = 80

# Preference parameters
par.beta = 0.96
par.sigma = 1.0
if par.sigma == 1:
    par.util = lambda c: mlog(c)
else:
    par.util = lambda c: (np.power(c, 1-par.sigma)-1) / (1-par.sigma)

# Labor endowment process
par.labor = ParamContainer()
par.labor.values = np.array((0.00, 0.75, 1.00, 1.25, 10.00))
par.labor.tm = np.array([[0.50, 0.20, 0.20, 0.10, 0.00],
                       [0.01, 0.80, 0.10, 0.09, 0.00],
                       [0.01, 0.04, 0.90, 0.05, 0.00],
                       [0.01, 0.00, 0.04, 0.90, 0.05],
                       [0.01, 0.09, 0.20, 0.20, 0.50]])

assert np.all(np.abs(np.sum(par.labor.tm, axis=1) - 1) < 1e-9)
# Initialize l_inv=0, will be updated later
par.labor.l_inv = [0, 0, 0, 0, 0]

# Taxes and benefits
par.taxes = ParamContainer()
par.taxes.benefits = np.array((0.25, 0, 0, 0, 0))
# Initialize tau=0, will be updated later to gov balance value
par.taxes.tau = 0

# Asset grid parameters
par.grid_a = ParamContainer()
par.grid_a.n = 6000
par.grid_a.logmin = -3
par.grid_a.logmax = 3

# Parameters to use if log-lin grid
par.grid_a.logshare = .8
par.grid_a.max = 1000
par.grid_a.min = 0.0
par.grid_a.logshift = 1


