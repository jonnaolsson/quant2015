from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from huggett.envir import resdir
import pickle
import numpy as np
from huggett.params import par
from huggett.initialize import initialize
from huggett.iterate import iterate_het
from huggett.distr import comp_dist
from huggett.results import Results
from huggett.transition import trans_path
from time import perf_counter


def optimize(a_grid, omega0, v0, g0, q):
    m_tot = par.m_tot
    pl = par.labor
    t0 = perf_counter()

    # Solve HHP for given prices
    t1 = perf_counter()
    v, g = iterate_het(a_grid, par, q, v0, g0, tol=1e-7)
    t2 = perf_counter()
    msg = '>>> VFI finished ({:.1f} sec)'
    print(msg.format(t2-t1))

    # Solve for invariate distribution
    t1 = perf_counter()
    omega_inv = comp_dist(omega0, a_grid, g, pl, tol=1e-7)
    t2 = perf_counter()
    msg = '>>> Compute distribution finished ({:.1f} sec)'
    print(msg.format(t2-t1))

    # Compute aggregate real savings acc. to HHP
    real_sav_tot = np.sum(omega_inv * a_grid)
    p_inv_eq = real_sav_tot / m_tot
    msg = '>>> Real savings total: {:g}, p_inv_eq= {:g}'
    print(msg.format(real_sav_tot, p_inv_eq))

    msg = '>>> Optimize done, total time {:.1f} sec'
    t3 = perf_counter()
    print(msg.format(t3-t0))

    return p_inv_eq, omega_inv, v, g


def calc_autarky(a_grid, omega0, v0, g0, tol=1e-6, maxiter=10):
    # The autarky q we solve for analytically
    # Calculating q_aut for the richest Jennifer Lawrence
    c = par.labor.values[4]*(1-par.taxes.tau)
    tm = par.labor.tm[4]
    income_next = par.labor.values*(1-par.taxes.tau) + par.taxes.benefits
    uprime_next = income_next**(-par.sigma)
    exp_uprime_next = np.dot(tm, uprime_next)
    q_aut = par.beta * exp_uprime_next / (c**-par.sigma)
    # Run it through the optimize to get the vfuns and pfuns
    for it in range(maxiter):
        p_inv_eq, omega_aut, vfun_aut, gfun_aut \
            = optimize(a_grid, omega0, v0, g0, q_aut)
        if p_inv_eq < tol:
            break
        else:
            print('Something wrong with the calc of p_aut!')
            q_aut += 0.01

    return q_aut, omega_aut, vfun_aut, gfun_aut


def main():

    print('\n*** Start computing equilibria *** \n')
    t_start = perf_counter()
    # Initialize values, grids, etc
    a_grid, omega0, omega_drop_a, omega_drop_b, v0, g0 = initialize(par)

    # Calculate autarky equilibrium
    q_aut, omega_aut, vfun_aut, gfun_aut = calc_autarky(a_grid, omega0, v0, g0)
    msg = 'Autarky calculated, q={:.4f} \n'
    print(msg.format(q_aut))

    # Calculate fiat money equilibrium
    q0 = 1.0
    p_inv_eq, omega_meq, v_meq, g_meq = optimize(a_grid, omega0, v0, g0, q0)
    msg = 'Money eq. calculated, p_inv_eq={:.4f} \n'
    print(msg.format(p_inv_eq))

    t1 = perf_counter()
    msg = '*** Finished computing equilibria ({:.1f} sec) *** \n'
    print(msg.format(t1-t_start))

    if par.trans_path:
        # Calculate the transition path case b (drop on poor)
        print('*** Start computing money drop on the poor *** \n')
        p_inv_path = np.linspace(p_inv_eq-1, p_inv_eq, par.big_t)
        p_inv_path_b, omega_path_b, vfun_path_b, g_path_b, eps_b \
            = trans_path(a_grid, p_inv_eq, v_meq, omega0, v0, g0,
                         p_inv_path, drop_method=2)

        # Calculate the transition path case a (drop on everyone)
        print('*** Start computing money drop on everyone *** \n')
        p_inv_path = np.linspace(p_inv_eq-1, p_inv_eq, par.big_t)
        p_inv_path_a, omega_path_a, vfun_path_a, g_path_a, eps_a \
            = trans_path(a_grid, p_inv_eq, v_meq, omega0, v0, g0,
                         p_inv_path, drop_method=1)

    res = Results()
    # Store the results from autarky
    res.q_aut = q_aut
    res.omega_aut = omega_aut
    res.vfun_aut = vfun_aut
    res.gfun_aut = gfun_aut
    # Store the results from equilibrium
    res.p_inv_eq = p_inv_eq
    res.omega = omega_meq
    res.vfun = v_meq
    res.pfun = g_meq

    if par.trans_path:
        # Store transition results from case a (dropping on everyone)
        res.p_inv_path_a = p_inv_path_a
        res.omega_path_a = omega_path_a
        res.vfun_path_a = vfun_path_a
        res.pfun_path_a = g_path_a
        res.convergence_a = eps_a
        res.omega_drop_a = omega_drop_a
        # Store transition results from case b (dropping on the poor)
        res.p_inv_path_b = p_inv_path_b
        res.omega_path_b = omega_path_b
        res.vfun_path_b = vfun_path_b
        res.pfun_path_b = g_path_b
        res.convergence_b = eps_b
        res.omega_drop_b = omega_drop_b

    # Remove the util function since you cannot pickle lambda functions
    par.util = None
    res.par = par
    # Pickle and save for future
    filen = 'res_huggett_trans_sigma%.d.bin' % res.par.sigma
    fn = join(resdir, filen)
    pickle.dump(res, open(fn, 'wb'))

    t_end = perf_counter()
    msg = 'Finished running, total time since start: {:.1f} min'
    print(msg.format((t_end - t_start)/60))


if __name__ == '__main__':
    main()