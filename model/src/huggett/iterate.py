from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np
from numba import jit


def iterate_het(a_grid, par, q, v0, g0, maxiter=1500, tol=1e-6):
    tm_m = par.labor.tm
    labor = par.labor.values
    tau = par.taxes.tau
    benefits = par.taxes.benefits
    beta = par.beta
    util = par.util
    anum = a_grid.shape[0]
    lnum = par.labor.values.shape[0]
    accelerator = par.accel
    eps = 1

    # Initialize value functions
    v_n = np.array(v0, copy=True)
    v_n1 = np.empty_like(v_n)

    # Initialize policy function
    g = np.array(g0, copy=True)

    # Precompute resources available at each gridpoint
    income = labor*(1-tau) + benefits
    resources = income.reshape((-1, 1)) + a_grid.reshape((1, -1))

    for it in range(maxiter):
        if not(it % 10 == 0) and accelerator == 1:
            for il_from in range(lnum):
                tm = tm_m[il_from]
                exp_v = np.dot(tm, v_n)
                for a_from in range(anum):
                    ia_to = g[il_from, a_from]
                    a_to = a_grid[ia_to]
                    c = resources[il_from, a_from]-q*a_to
                    v_n1[il_from, a_from] = util(c) + beta*exp_v[ia_to]
        else:
            for il_from in range(lnum):
                ap = 0
                tm = tm_m[il_from]
                exp_v = np.dot(tm, v_n)
                for ia_from in range(anum):
                    vmax = -np.inf
                    res_a = resources[il_from, ia_from]
                    for ap in range(ap, anum):
                        a_to = a_grid[ap]
                        if a_to*q > res_a:
                            break
                        v = util(res_a-q*a_to) + beta*exp_v[ap]
                        if v < vmax:
                            ap -= 1
                            break
                        vmax = v
                    v_n1[il_from, ia_from] = vmax
                    g[il_from, ia_from] = ap
            # msg = 'VFI: iteration {:3d}, eps={:g}'
            # print(msg.format(it, eps))

        eps = np.max(np.abs(v_n-v_n1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break

        v_n, v_n1 = v_n1, v_n
    else:
        msg = 'No convergence in iterate_het, eps={:g}'
        raise Exception(msg.format(eps))

    return v_n1, g


def iterate_trans(a_grid, par, v0, g0, v_next, q):
    tm_m = par.labor.tm
    labor = par.labor.values
    tau = par.taxes.tau
    benefits = par.taxes.benefits
    beta = par.beta
    util = par.util
    anum = a_grid.shape[0]
    lnum = par.labor.values.shape[0]

    # Initialize value functions
    v_n1 = np.empty_like(v0)

    # Initialize policy function
    g = g0

    # Precompute resources available at each gridpoint
    income = labor*(1-tau) + benefits
    resources = income.reshape((-1, 1)) + a_grid.reshape((1, -1))

    for il_from in range(lnum):
        ap = 0
        tm = tm_m[il_from]
        # Note use of v_next, i.e. the vf for next period!
        exp_v = np.dot(tm, v_next)
        if par.sigma == 1:
            v_n1, g = iterate_trans_n_log(resources, a_grid, g, v_n1, anum,
                                          il_from, beta, exp_v, q, ap)
        else:
            v_n1, g = iterate_trans_n_nonlog(resources, a_grid, g, v_n1,
                                             anum, il_from, beta, exp_v, q, ap,
                                             par.sigma)

    v = v_n1
    g_t = g

    return g_t, v


@jit
def iterate_trans_n_log(resources, a_grid, g, v_n1, anum, il_from, beta,
                        exp_v, q, ap):
    for ia_from in range(anum):
        vmax = -np.inf
        res_a = resources[il_from, ia_from]
        for ap in range(ap, anum):
            a_to = a_grid[ap]
            if a_to*q > res_a:
                break
            v = np.log(res_a-q*a_to) + beta*exp_v[ap]
            if v < vmax:
                ap -= 1
                break
            vmax = v
        v_n1[il_from, ia_from] = vmax
        g[il_from, ia_from] = ap
    return v_n1, g


@jit
def iterate_trans_n_nonlog(resources, a_grid, g, v_n1, anum, il_from,
                           beta, exp_v, q, ap, sigma):
    for ia_from in range(anum):
        vmax = -np.inf
        res_a = resources[il_from, ia_from]
        for ap in range(ap, anum):
            a_to = a_grid[ap]
            if a_to*q > res_a:
                break
            v = (np.power(res_a-q*a_to, 1-sigma)-1) / (1-sigma) + beta*exp_v[ap]
            if v < vmax:
                ap -= 1
                break
            vmax = v
        v_n1[il_from, ia_from] = vmax
        g[il_from, ia_from] = ap
    return v_n1, g