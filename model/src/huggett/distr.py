from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np
from numba import jit


def comp_dist(omega0, a_grid, g, pl, maxiter=500, tol=1e-6):
    eps = 1
    tm_m = pl.tm
    anum = a_grid.shape[0]
    lnum = pl.values.shape[0]

    omega0 = np.array(omega0, copy=True)
    omega = np.zeros((lnum, anum))

    for it in range(maxiter):
        omega[:] = 0.0
        for l in range(lnum):
            for a in range(anum):
                ap = g[l, a]
                for lp in range(lnum):
                    omega[lp, ap] = omega[lp, ap] + tm_m[l, lp]*omega0[l, a]
        eps = np.max(np.abs(omega-omega0))
        if eps < (tol*np.max(omega)):
            break
        omega, omega0 = omega0, omega

    else:
        msg = 'No convergence in comp_dist, eps={:g}'
        raise Exception(msg.format(eps))

    return omega


def upd_dist(omega0, a_grid, g, pl):
    tm_m = pl.tm
    anum = a_grid.shape[0]
    lnum = pl.values.shape[0]
    omega = np.zeros((lnum, anum))
    omega = upd_dist_n(omega0, omega, lnum, anum, tm_m, g)
    # for l in range(lnum):
    #     for a in range(anum):
    #         ap = g[l, a]
    #         for lp in range(lnum):
    #             omega[lp, ap] = omega[lp, ap] + tm_m[l, lp]*omega0[l, a]
    return omega


@jit
def upd_dist_n(omega0, omega, lnum, anum, tm_m, g):
    for l in range(lnum):
        for a in range(anum):
            ap = g[l, a]
            for lp in range(lnum):
                omega[lp, ap] = omega[lp, ap] + tm_m[l, lp]*omega0[l, a]

    return omega