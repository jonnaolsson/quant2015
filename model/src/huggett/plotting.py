from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import matplotlib.pyplot as plt
import numpy as np

def plot_gen(x, y, mult_lin, title, xlab, ylab, fname, cols, llabs='',
             legloc='lower right', text='', xlim=[0, 1], ylim=[0, 1]):
    plt.gca().set_color_cycle(cols)

    if mult_lin:
        for i in range(y.shape[0]):
            fig = plt.plot(x, y[i, :], lw=2.0, label=llabs[i])
    else:
        fig = plt.plot(x, y, lw=2, label=llabs)

    plt.legend(loc=legloc)
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.text(.02 * np.max(x), .9 * np.max(y), text)
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.grid(True)
    plt.savefig(fname, format='PDF')
    plt.show(fig)
    plt.close()


def plot_lorenz(x0, y0, x1, y1, title, xlab, ylab, fname, cols,
                llabs='', legloc='lower right', text='',
                xlim=[0, 1], ylim=[0, 1]):
    plt.gca().set_color_cycle(cols)
    fig = plt.plot(x0, y0, lw=2.0, label=llabs[0])
    plt.plot(x1, y1, lw=2.0, label=llabs[1])
    plt.legend(loc=legloc)
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.text(.02 * np.max(x1), .9 * np.max(y1), text)
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.grid(True)
    plt.savefig(fname, format='PDF')
    plt.show(fig)
    plt.close()