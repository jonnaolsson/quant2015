from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from huggett.envir import resdir, plotdir
from huggett.analyze import calc_ineq, analyze_trans
from common.plotting import plot_gen, plot_lorenz
import pickle
import numpy as np
import matplotlib.pyplot as plt


def load_data():
    fn = join(resdir, 'res_huggett_trans_sigma1.bin')
    res = pickle.load(open(fn, 'rb'))

    # Recreate asset grid
    pg = res.par.grid_a
    correction = 10 ** pg.logmin
    a = np.logspace(pg.logmin, pg.logmax, pg.n) - correction

    # Find distribution of wealth (per grid point and cumulative)
    a_dist = np.sum(res.omega, axis=0)

    return res, a, a_dist


def fix_colors():
    colcyc = ['#ff7f00', '#377eb8', '#4daf4a', '#984ea3', '#e41a1c']
    return colcyc


def plot_m_eq(res, a, a_dist):

    # Create grid of actual asset choices
    anext = a[res.pfun]
    colcyc = fix_colors()

    # Plot value functions
    text = 'Price in equilibrium: %.4f' % res.p_inv_eq
    fn = 'huggett_meq_vfun_sigma%.d.pdf' % res.par.sigma
    plot_gen(a, res.vfun,
             mult_lin=True,
             title='Value function (money equilibrium)',
             xlab='Assets', ylab='Value',
             xlim=[np.min(a), np.max(a)],
             ylim=[np.min(res.vfun), np.max(res.vfun*1.05)],
             fname=join(plotdir, fn),
             cols=colcyc,
             text=text,
             llabs=res.par.labor.values)
    # Plot policy function
    fn = 'huggett_meq_pfun_sigma%.d.pdf' % res.par.sigma
    plot_gen(a, anext,
             mult_lin=True,
             title='Policy function (money equilibrium)',
             xlab='Wealth (real)', ylab='Next period wealth (real) ',
             xlim=[np.min(a), np.max(a)],
             ylim=[np.min(anext), np.max(anext)],
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=res.par.labor.values)
    # Plot distribution of money
    fn = 'huggett_meq_dist_sigma%.d.pdf' % res.par.sigma
    plot_gen(a, a_dist,
             mult_lin=False,
             title='Distribution of money',
             xlab='Money', ylab='Density',
             xlim=[np.min(a), 100],
             ylim=[np.min(a_dist), 0.02],
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs='Frequency')


def plot_aut_eq(res, a):

    # Create grid of actual asset choices
    anext = a[res.gfun_aut]

    colcyc = fix_colors()
    text = 'Shadow q in equilibrium: %.4f' % res.q_aut
    fn = 'huggett_aut_vfun_sigma%.d.pdf' % res.par.sigma
    plot_gen(a, res.vfun_aut,
             mult_lin=True,
             title='Value function (autarky)',
             xlab='Assets', ylab='Value',
             xlim=[np.min(a), np.max(a)],
             ylim=[np.min(res.vfun_aut), np.max(res.vfun_aut)*1.05],
             fname=join(plotdir, fn),
             cols=colcyc,
             text=text,
             llabs=res.par.labor.values)

    fn = 'huggett_aut_pfun_sigma%.d.pdf' % res.par.sigma
    plot_gen(a, anext,
             mult_lin=True,
             title='Policy function (autarky)',
             xlab='Wealth (real)', ylab='Next period wealth (real) ',
             xlim=[np.min(a), np.max(a)],
             ylim=[np.min(anext), np.max(anext)],
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=res.par.labor.values)


def main():
    print('Start plot_res')
    res, a, a_dist = load_data()
    colors = fix_colors()

    msg = 'Number of grid points {:3d}'
    print(msg.format(a.shape[0]))

    # Plot value functions for autarky
    plot_aut_eq(res, a)

    # Plot value functions money equilibrium
    plot_m_eq(res, a, a_dist)

    # Calculate and plot inequality measures (Lorenz and Gini)
    calc_ineq(res, a, colcyc=colors)

    # Analyze and plot transition paths
    analyze_trans(res, a, colcyc=colors, print_details=False)

    msg = '\nEquilibrium tax rate: {:.3f} percent'
    print(msg.format(res.par.taxes.tau*100))

    msg = '\nAutarky interest rate: {:.3f} percent'
    print(msg.format((1/res.q_aut-1)*100))

    print('\nInvariate distribution is the following (in percent): ')
    print(res.par.labor.l_inv*100)

    print('Finished plot_res')


if __name__ == '__main__':
    main()
