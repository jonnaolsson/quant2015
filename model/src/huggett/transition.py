from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np
from huggett.params import par
from huggett.initialize import calc_omega_drop
from huggett.iterate import iterate_trans
from huggett.distr import upd_dist
from time import perf_counter


def trans_vfun(a_grid, g0, v0, p_inv_path, p_inv_eq, v_eq):
    g_path = np.zeros((g0.shape[0], g0.shape[1], par.big_t))
    vfun_path = np.zeros((v0.shape[0], v0.shape[1], par.big_t))
    for it in range(par.big_t):
        t = par.big_t - it - 1
        if t == par.big_t - 1:
            vfun_next = v_eq
            q = p_inv_path[t] / p_inv_eq
        else:
            vfun_next = vfun_path[:, :, t+1]
            q = p_inv_path[t] / p_inv_path[t+1]
        g_path[:, :, t], vfun_path[:, :, t] = iterate_trans(a_grid, par, v0,
                                                            g0, vfun_next, q)
    return g_path, vfun_path


def trans_omega(a_grid, omega_drop, g_path):
    omega_path = np.zeros((omega_drop.shape[0], omega_drop.shape[1], par.big_t))
    p_update = np.zeros(par.big_t)
    for it in range(par.big_t):
        t = it
        g_t = g_path[:, :, t]
        if t == 0:
            omega_in = omega_drop
        else:
            omega_in = omega_path[:, :, t-1]
        omega_out = upd_dist(omega_in, a_grid, g_t, par.labor)
        check = np.sum(np.sum(omega_out))
        if np.abs(check-1) > 1e-6:
            msg = 'Total mass: {:.4f}, t={:g}'
            print(msg.format(check, t))
        real_sav_tot = np.sum(omega_out * a_grid)
        p_update[t] = real_sav_tot / par.m_tot
        omega_path[:, :, t] = omega_out
    return p_update, omega_path


def update_p_inv(p_inv_path, p_inv_update, it):
    eps = np.max(np.abs(p_inv_update - p_inv_path))
    msg = 'Max abs difference in price path: {:.6f}'
    print(msg.format(eps))
    if it < 150:
        corr = np.max([0.1, 0.6/((it+1)**0.5)])
    else:
        corr = 0.08
    msg = '(correction = {:g})'
    print(msg.format(corr))
    p_inv_new = (1-corr)*p_inv_path + corr*p_inv_update
    if it > 1480:
        print(p_inv_path)
        print(p_inv_update)
        print(p_inv_new)
    return p_inv_new, eps


def trans_path(a_grid, p_inv_eq, v_eq, omega0, v0, g0,
               p_inv_path, drop_method, maxiter=1500, tol=1e-4):
    msg = '*** Computing transition path *** \n'
    print(msg)
    omega_path = np.zeros((omega0.shape[0], omega0.shape[1], par.big_t))
    vfun_path = np.zeros((v0.shape[0], v0.shape[1], par.big_t))
    g_path = np.zeros((g0.shape[0], g0.shape[1], par.big_t))
    t_start = perf_counter()
    for it in range(maxiter):
        msg = '*** Starting iteration {:g} ***'
        print(msg.format(it))
        t0 = perf_counter()

        # Update policy functions and value functions from behind
        g_path, vfun_path = trans_vfun(a_grid, g0, v0, p_inv_path, p_inv_eq,
                                       v_eq)
        t1 = perf_counter()
        msg = 'Policy functions done, time: {:.1f} s'
        print(msg.format(t1-t0))

        # Update price path and omega path from start
        # Note that omega_drop originally is in nominal terms
        # We have to adjust it to real terms according to current guess
        omega_drop = calc_omega_drop(par, a_grid, par.labor.l_inv, drop_method,
                                     p_inv_path[0])

        p_inv_update, omega_path = trans_omega(a_grid, omega_drop, g_path)

        # Update price path and see how big the difference is atm
        p_inv_new, eps = update_p_inv(p_inv_path, p_inv_update, it)
        if eps < tol:
            break
        p_inv_path = p_inv_new
        t_it_end = perf_counter()
        msg = 'Time since start of transition path calc: {:.1f} min \n'
        print(msg.format((t_it_end-t_start)/60))

    print('*** Transition path finished *** \n')
    return p_inv_path, omega_path, vfun_path, g_path, eps
