from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from common.plotting import plot_gen, plot_lorenz
from huggett.envir import resdir, plotdir
from huggett.initialize import calc_omega_drop
import numpy as np


def calc_ineq(res, a, colcyc):
    a_pdf = np.sum(res.omega, axis=0)
    people_cum = np.cumsum(a_pdf)
    a_per_bracket = a * a_pdf
    a_cum = np.cumsum(a_per_bracket)

    # Calculating the Gini coefficient using wikipedia formula for
    # discrete pdf: G = 1 - (sum(f(y)(S_(i-1) + S_i))/(S_n)
    # where S_i = sum(f(y)y_i), i.e. a_cum
    numerator = 0.0
    for i in range(a.shape[0]):
        if i == 0:
            numerator += a_pdf[i] * (a_cum[i])
        else:
            numerator += a_pdf[i] * (a_cum[i - 1] + a_cum[i])
    gini_wealth = 1 - numerator / a_cum[-1]

    # Find distribution of people per income class
    labor = res.par.labor.values
    l_pdf = np.sum(res.omega, 1)
    l_people_cum = np.cumsum(l_pdf)
    # Note - pre-transfer and taxes!
    l_per_bracket = labor * l_pdf
    l_cum = np.cumsum(l_per_bracket)

    # Calculating the Gini for income
    numerator = 0.0
    for i in range(labor.shape[0]):
        if i == 0:
            numerator += l_pdf[i] * (l_cum[i])
        else:
            numerator += l_pdf[i] * (l_cum[i - 1] + l_cum[i])
    gini_income = 1 - numerator / l_cum[-1]

    # Plot Lorenz for autarky
    labels = ['Wealth', 'Income']
    text = 'Gini Wealth: N/A \nGini Income: %.3f' % gini_income
    fn = 'huggett_aut_lorenz_sigma%.d.pdf' % res.par.sigma
    plot_lorenz(people_cum, np.zeros((people_cum.shape[0], 1)),
                l_people_cum, l_cum/l_cum[-1],
                title='Lorenz curves autarky',
                xlab='Fraction of population',
                ylab='Cumulative share',
                fname=join(plotdir, fn),
                cols=colcyc,
                llabs=labels,
                text=text)

    # Plot Lorenz for money eq.
    labels = ['Wealth', 'Income']
    text = 'Gini Wealth: %.3f \nGini Income: %.3f' % (gini_wealth, gini_income)
    fn = 'huggett_meq_lorenz_sigma%.d.pdf' % res.par.sigma
    plot_lorenz(people_cum, a_cum/a_cum[-1],
                l_people_cum, l_cum/l_cum[-1],
                title='Lorenz curves money equilibrium',
                xlab='Fraction of population',
                ylab='Cumulative share',
                fname=join(plotdir, fn),
                cols=colcyc,
                llabs=labels,
                text=text)


def analyze_trans(res, a, colcyc, print_details=False):
    big_t = res.par.big_t
    p_inv_path = np.zeros((2, big_t))
    p_inv_path[0, :] = res.p_inv_path_a
    p_inv_path[1, :] = res.p_inv_path_b
    t = np.linspace(1, big_t, big_t)
    text = 'Note: price of money!'
    labels = ['Case a (everyone gets money)', 'Case b (money given to the '
                                              'unemployed)']
    fn = 'huggett_m_p_inv_path_sigma%.d.pdf' % res.par.sigma
    plot_gen(t, p_inv_path,
             mult_lin=True,
             title='Price path during transition',
             xlab='time periods',
             ylab='p_inv (price of money in terms of goods)',
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=labels,
             legloc='upper right',
             text=text,
             xlim=[1, big_t],
             ylim=[np.min(p_inv_path), np.max(p_inv_path)*1.1])

    p_path = 1 / p_inv_path
    fn = 'huggett_m_p_path_sigma%.d.pdf' % res.par.sigma
    plot_gen(t, p_path,
             mult_lin=True,
             title='Price level path during transition',
             xlab='time periods',
             ylab='p (price level)',
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=labels,
             legloc='upper right',
             xlim=[1, big_t],
             ylim=[np.min(p_path)*.95, np.max(p_path)])

    q = np.zeros((2, big_t))
    for i in range(big_t):
        if i == big_t-1:
            q[0, i] = p_inv_path[0, i] / res.p_inv_eq
            q[1, i] = p_inv_path[1, i] / res.p_inv_eq
        else:
            q[0, i] = p_inv_path[0, i] / p_inv_path[0, i+1]
            q[1, i] = p_inv_path[1, i] / p_inv_path[1, i+1]

    fn = 'huggett_m_q_path_sigma%.d.pdf' % res.par.sigma
    plot_gen(t, q,
             mult_lin=True,
             title='q during transition',
             xlab='time periods',
             ylab='q',
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=labels,
             legloc='upper right',
             xlim=[1, big_t],
             ylim=[np.min(q)*0.99, np.max(q)*1.01])

    # Check distributions
    omega_path_a = res.omega_path_a[:, :, -1]
    omega_path_b = res.omega_path_b[:, :, -1]
    diff_a = np.max(np.abs(res.omega - omega_path_a))
    diff_b = np.max(np.abs(res.omega - omega_path_b))
    msg = '\nMax diff in distr, case a: {:.4f}, case b: {:.4f}\n'
    print(msg.format(diff_a, diff_b))
    # Print if necessary...
    if print_details:
        argmax_diff_a = np.argmax(np.abs(res.omega - omega_path_a), axis=1)
        argmax_diff_b = np.argmax(np.abs(res.omega - omega_path_b), axis=1)
        print('Grid point indices for max difference per labour group (a and b)')
        print(argmax_diff_a, argmax_diff_b)
        for i in range(argmax_diff_a.shape[0]):
            print('\nCase a, labor value %.2f' % res.par.labor.values[i])
            print('A grid value: %2f' % a[argmax_diff_a[i]])
            print('Omega: %.4f' % res.omega[i, argmax_diff_a[i]])
            print('Omega_trans_path_a: %.4f' % omega_path_a[i, argmax_diff_a[i]])

    a_dist_comp = np.zeros((3, a.shape[0]))
    a_dist_comp[0, :] = np.sum(omega_path_a, axis=0)
    a_dist_comp[1, :] = np.sum(omega_path_b, axis=0)
    a_dist_comp[2, :] = np.sum(res.omega, axis=0)
    labels = ['End of trans path case a', 'End of trans path case b',
              'Money eq.']

    fn = 'huggett_trans_dist_sigma%.d.pdf' % res.par.sigma
    plot_gen(a, a_dist_comp,
             mult_lin=True,
             title='Distribution of money',
             xlab='Real savings', ylab='Density',
             xlim=[np.min(a), 20],
             ylim=[np.min(a_dist_comp), 0.02],
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=labels,
             legloc='upper right')

    bin_n = 6000
    bins = np.zeros((3, bin_n))

    bins[0, :], foo = np.histogram(a, bins=bin_n, weights=a_dist_comp[0, :],
                                   density=True)
    bins[1, :], foo = np.histogram(a, bins=bin_n, weights=a_dist_comp[1, :],
                                   density=True)
    bins[2, :], foo = np.histogram(a, bins=bin_n, weights=a_dist_comp[2, :],
                                   density=True)

    fn = 'huggett_trans_distbin_sigma%.d.pdf' % res.par.sigma
    plot_gen(foo[:bin_n], bins,
             mult_lin=True,
             title='Distribution of money',
             xlab='Real savings', ylab='Density',
             xlim=[np.min(a), 30],
             ylim=[np.min(bins), 0.2],
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=labels,
             legloc='upper right',
             linewidth=0.5)

    comp_vf_trans(res, a, print_details)


def comp_vf_trans(res, a, print_detail=False):
    # Comparing value functions
    # Autarky is easy - everyone has 0 money
    vfun_aut_actual = res.vfun_aut[:, 0]
    vfun_per_gp = res.omega_aut * res.vfun_aut
    print('\nValues in autarky: ')
    print(vfun_aut_actual)
    msg = 'Total value: {:.3f}'
    print(msg.format(np.sum(vfun_per_gp)))

    # Money equilibrium - weighing by wherever they are
    lab_shares = np.sum(res.omega, axis=1)
    vfun_per_gp = res.omega * res.vfun
    vfun_meq_actual = np.sum(vfun_per_gp, axis=1) / lab_shares
    print('\nValues in money eq: ')
    print(vfun_meq_actual)
    msg = 'Total value: {:.3f}'
    print(msg.format(np.sum(vfun_per_gp)))

    # Start analyzing transition case a - everyone gets something
    # Money drop case a - everyone gets something
    omega_drop_a = calc_omega_drop(res.par, a, res.par.labor.l_inv, 1,
                                   res.p_inv_path_a[0])
    vfun_per_gp = omega_drop_a * res.vfun_path_a[:, :, 0]
    vfun_transa_actual = np.sum(vfun_per_gp, axis=1) / lab_shares
    print('\nValues in t=0, transition case a:')
    print(vfun_transa_actual)
    msg = 'Total value: {:.3f}'
    print(msg.format(np.sum(vfun_per_gp)))

    # Money drop case b - the poor get all
    omega_drop_b = calc_omega_drop(res.par, a, res.par.labor.l_inv, 2,
                                   res.p_inv_path_a[0])
    vfun_per_gp = omega_drop_b * res.vfun_path_a[:, :, 0]
    vfun_transb_actual = np.sum(vfun_per_gp, axis=1) / lab_shares
    print('\nValues in t=0, transition case b:')
    print(vfun_transb_actual)
    msg = 'Total value: {:.3f}'
    print(msg.format(np.sum(vfun_per_gp)))

    if print_detail:
        print('\nCalculating savings shares for money equilibrium')
        calc_sav_shares(res.omega, a)
        print('\nCalculating savings shares for money drop case a')
        calc_sav_shares(omega_drop_a, a)
        print('\nCalculating savings shares for money drop case b')
        calc_sav_shares(omega_drop_b, a)


def calc_sav_shares(omega, a):
    lab_shares = np.sum(omega, axis=1)
    sav_per_lgroup = np.sum(omega*a, axis=1)
    share_of_sav = sav_per_lgroup / np.sum(sav_per_lgroup)
    w_pcap_meq = sav_per_lgroup / lab_shares
    msg = '\nReal savings per lab group: ({:.3f}) ({:.3f}) ({' \
          ':.3f}) ({:.3f}) ({:.3f})'
    print(msg.format(sav_per_lgroup[0], sav_per_lgroup[1], sav_per_lgroup[2],
                     sav_per_lgroup[3], sav_per_lgroup[4]))
    msg = 'Share of savings per lab group: ({:.3f}) ({:.3f}) ({:.3f}) ({:.3f}) ' \
          '({:.3f})'
    print(msg.format(share_of_sav[0], share_of_sav[1], share_of_sav[2],
                     share_of_sav[3], share_of_sav[4]))
    msg = 'Share of population: ({:.3f}) ({:.3f}) ({:.3f}) ({:.3f}) ' \
          '({:.3f})'
    print(msg.format(lab_shares[0], lab_shares[1], lab_shares[2],
                     lab_shares[3], lab_shares[4]))
    msg = 'Per capita real savings per lab group: ({:.3f}) ({:.3f}) ({' \
          ':.3f}) ({:.3f}) ({:.3f})\n'
    print(msg.format(w_pcap_meq[0], w_pcap_meq[1], w_pcap_meq[2],
                     w_pcap_meq[3], w_pcap_meq[4]))