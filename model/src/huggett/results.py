from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'


class Results:
    def __init__(self):
        self.par = None

        self.q_aut = None
        self.vfun_aut = None
        self.omega_aut = None
        self.gfun_aut = None

        self.q_eq = 0.0
        self.p_inv_eq = None
        self.omega = None
        self.vfun = None
        self.pfun = None

        self.p_inv_path_a = None
        self.omega_path_a = None
        self.vfun_path_a = None
        self.pfun_path_a = None
        self.omega_drop_a = None
        self.convergence_a = None

        self.p_inv_path_b = None
        self.omega_path_b = None
        self.vfun_path_b = None
        self.pfun_path_b = None
        self.omega_drop_b = None
        self.convergence_b = None

