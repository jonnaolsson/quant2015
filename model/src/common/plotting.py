from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import matplotlib.pyplot as plt
import numpy as np


def plot_gen(x, y, mult_lin, title, xlab, ylab, fname, cols, llabs='',
             legloc='lower right', text='', xlim=[0, 1], ylim=[0, 1],
             linewidth=2.0):
    plt.gca().set_color_cycle(cols)

    if mult_lin:
        for i in range(y.shape[0]):
            fig = plt.plot(x, y[i, :], lw=linewidth, label=llabs[i])
    else:
        fig = plt.plot(x, y, lw=linewidth, label=llabs)

    plt.legend(loc=legloc)
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.text(.02 * np.max(x), .9 * ylim[1], text)
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.grid(True)
    plt.savefig(fname, format='PDF')
    #plt.show(fig)
    plt.close()


def plot_cons(x, y, mult_lin, title, xlab, ylab, fname, cols, llabs='',
             legloc='lower right', text='', xlim=[0, 1], ylim=[0, 1],
             linewidth=2.0):
    plt.gca().set_color_cycle(cols)

    if mult_lin:
        for i in range(y.shape[0]):
            fig = plt.plot(x[i, :], y[i, :], lw=linewidth, label=llabs[i])
    else:
        fig = plt.plot(x, y, lw=linewidth, label=llabs)

    plt.legend(loc=legloc)
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.text(.02 * np.max(x), .9 * ylim[1], text)
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.grid(True)
    plt.savefig(fname, format='PDF')
    #plt.show(fig)
    plt.close()


def plot_lorenz(x0, y0, x1, y1, title, xlab, ylab, fname, cols,
                llabs='', legloc='lower right', text='',
                xlim=[0, 1], ylim=[0, 1]):
    plt.gca().set_color_cycle(cols)
    fig = plt.plot(x0, y0, '-', lw=2.0, label=llabs[0])
    plt.plot(x1, y1, '--', lw=2.0, label=llabs[1])
    plt.legend(loc=legloc)
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.text(.02 * np.max(x1), .9 * np.max(y1), text)
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.grid(True)
    plt.savefig(fname, format='PDF')
    #plt.show(fig)
    plt.close()


def plot_lorenz3(x0, y0, x1, y1, x2, y2, title, xlab, ylab, fname, cols,
                 llabs='', legloc='upper left', text='',
                 xlim=[0, 1], ylim=[0, 1]):
    plt.gca().set_color_cycle(cols)
    fig = plt.plot(x0, y0, lw=2.0, label=llabs[0])
    plt.plot(x1, y1, lw=2.0, label=llabs[1])
    plt.plot(x2, y2, lw=2.0, label=llabs[2])
    plt.legend(loc=legloc)
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.text(.62 * np.max(x1), .1 * np.max(y1), text)
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.grid(True)
    plt.savefig(fname, format='PDF')
    #plt.show(fig)
    plt.close()


def get_filename(res, model, graph, tfp=None, age=None, method=None,
                 nyy=None, comp=None):
    fn = model + '_' + graph + '_'
    extra = 'sigma%.d_ex%.d'
    if tfp is not None:
        extra += '_tfp%.d'
        fn = fn + extra + '.pdf'
        filename = fn % (res.par.sigma, res.par.exercise, tfp)
    elif age is not None and nyy is not None:
        extra += '_age%.d' + '_nyy%.d'
        fn = fn + extra + '.pdf'
        filename = fn % (res.par.sigma, res.par.exercise, age, nyy)
    elif age is not None:
        extra += '_age%.d'
        fn = fn + extra + '.pdf'
        filename = fn % (res.par.sigma, res.par.exercise, age)
    elif method is not None and nyy is not None:
        extra += '_method_' + method + '_nyy%.d'
        fn = fn + extra + '.pdf'
        filename = fn % (res.par.sigma, res.par.exercise, nyy)
    elif method is not None:
        extra += '_method_' + method
        fn = fn + extra + '.pdf'
        filename = fn % (res.par.sigma, res.par.exercise)
    elif comp is not None and nyy is not None:
        extra += '_vs_ex%.d' + '_nyy%.d'
        fn = fn + extra + '.pdf'
        filename = fn % (res.par.sigma, res.par.exercise, comp, nyy)
    elif comp is not None:
        extra += '_vs_ex%.d'
        fn = fn + extra + '.pdf'
        filename = fn % (res.par.sigma, res.par.exercise, comp)
    else:
        fn = fn + extra + '.pdf'
        filename = fn % (res.par.sigma, res.par.exercise)

    return filename


def fix_colors(no=6):
    if no == 3:
        colcyc = ['#ff7f00', '#377eb8', '#4daf4a']
    else:
        colcyc = ['#ff7f00', '#377eb8', '#4daf4a', '#984ea3', '#e41a1c']
    return colcyc
