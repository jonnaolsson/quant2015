from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from lifecycle.envir import resdir
from time import perf_counter
import pickle
import numpy as np


def load_data(sigma, exercise):
    file = 'res_lc_sigma%.d_ex%.d.bin' % (sigma, exercise)
    fn = join(resdir, file)
    res = pickle.load(open(fn, 'rb'))
    return res


def get_sfun_xvals(res, x):
    return {
        'VFI': (res.sfun_vfi, res.par.grid.xx),
        'PFI': (res.sfun_pfi, res.par.grid.xx),
        'EGM': (res.sfun_egm, res.xfun_egm)
        }.get(x, 9)


def draw_shocks(par, sim_n):
    np.random.seed(par.simulation.seed)
    # Permanent shocks
    n_shock = np.ones((sim_n, par.big_t))
    n_shock[:, :par.t_ret] = np.random.lognormal(par.mu_n, np.sqrt(
        par.var_n), n_shock[:, :par.t_ret].size).reshape(sim_n, par.t_ret)

    # Transitory shocks
    v_shock = np.ones((sim_n, par.big_t))
    v_shock[:, :par.t_ret] = np.random.lognormal(par.mu_v, np.sqrt(
        par.var_v), n_shock[:, :par.t_ret].size).reshape(sim_n, par.t_ret)

    return n_shock, v_shock


def get_p_series(par, sim_n, n_shock):
    trend_g = par.trend_g
    p_series = np.ones((sim_n, par.big_t))
    for t in range(par.big_t):
        if t == 0:
            p_series[:, t] = trend_g[t] * n_shock[:, t]
        else:
            p_series[:, t] = p_series[:, t-1] * trend_g[t] * n_shock[:, t]
    return p_series


def get_total_inc(p_series, v_shock):
    inc_tot = p_series * v_shock
    return inc_tot


def prepare_simulation(res):
    sim_n = res.par.simulation.n

    # Get random shock and series
    n_shock, v_shock = draw_shocks(res.par, sim_n)
    p_series = get_p_series(res.par, sim_n, n_shock)
    inc_tot = get_total_inc(p_series, v_shock)

    return inc_tot, n_shock, v_shock, p_series


def iterate(par, n_shock, v_shock, xvals, sfun, p_series, method):
    sim_n = par.simulation.n
    big_t = par.big_t
    trend_g = par.trend_g
    r = par.r
    ss = par.grid.ss

    # Initialize result matrices
    norm_x = np.zeros((sim_n, big_t))
    norm_s = np.zeros((sim_n, big_t))

    # Set starting values for first period
    norm_x[:, 0] = v_shock[:, 0]

    # Now iterate over periods
    if method == 'VFI' or method == 'PFI':
        for age in range(big_t-1):
            norm_s[:, age] = np.interp(norm_x[:, age], xvals, ss[sfun[:, age]])
            # Updating next period's cash-on-hand
            r_idx = np.array(norm_s[:, age] <= 0, dtype=np.uint32)
            norm_x[:, age+1] = (1+r[r_idx])*norm_s[:, age] / \
                               (trend_g[age+1]*n_shock[:, age]) + \
                               v_shock[:, age+1]

    elif method == 'EGM':
        for age in range(big_t-1):
            norm_s[:, age] = np.interp(norm_x[:, age], xvals[:, age], ss)
            # Updating next period's cash-on-hand
            r_idx = np.array(norm_s[:, age] <= 0, dtype=np.uint32)
            norm_x[:, age+1] = (1+r[r_idx])*norm_s[:, age] / \
                               (trend_g[age+1]*n_shock[:, age]) + \
                               v_shock[:, age+1]
    else:
        print('Error error error.... Unknown method.')

    # De-normalize
    x_final = norm_x * p_series
    s_final = norm_s * p_series

    return x_final, s_final


# Following function is used for exercise 5
def iterate5(par, n_shock, v_shock, xx_def, xx_nodef, vf_def, vf_nodef,
             sfun_def, sfun_nodef, ss_def, ss_nodef):
    sim_n = par.simulation.n
    big_t = par.big_t
    trend_g = par.trend_g
    r = par.r

    # Initialize result matrices
    norm_x = np.zeros((sim_n, big_t))
    norm_s = np.zeros((sim_n, big_t))
    default = np.zeros((sim_n, big_t), dtype=bool)

    # Set starting values for first period
    norm_x[:, 0] = v_shock[:, 0]

    t_start = perf_counter()
    msg = 'Starting simulations... '
    print(msg)

    # Now iterate over periods
    for age in range(big_t):
        if age > 0:
            # Compare value functions for defaulting or non-defaulting
            value_def = np.interp(v_shock[:, age], xx_def, vf_def[:, age])
            value_nodef = np.interp(norm_x[:, age], xx_nodef, vf_nodef[:, age])
            default_now = (value_def > value_nodef) & (~default[:, age - 1])

            # If default, reset norm_x to just income
            norm_x[default_now, age] = v_shock[default_now, age]

            # The defaulted are those defaulted last period plus the new ones
            default[:, age] = default[:, age-1] | default_now

        is_def = default[:, age]
        no_def = ~is_def
        # Find savings for the defaulted
        norm_s[is_def, age] = \
            np.interp(norm_x[is_def, age], xx_def, ss_def[sfun_def[:, age]])
        # Find savings for the non-defaulted
        norm_s[no_def, age] = np.interp(norm_x[no_def, age], xx_nodef,
                                        ss_nodef[sfun_nodef[:, age]])

        if age == big_t-1:
            break

        # Updating next period's cash-on-hand
        r_idx = np.array(norm_s[:, age] <= 0, dtype=np.uint32)
        norm_x[:, age+1] = (1+r[r_idx])*norm_s[:, age] / \
                           (trend_g[age+1]*n_shock[:, age]) + v_shock[:, age+1]

    t_end = perf_counter()
    msg = 'Simulations done in {:.1f} sec\n'
    print(msg.format(t_end-t_start))

    return norm_x, norm_s, default


def main_simulation(methods, sigma, exercise):
    res = load_data(sigma, exercise)

    res.methods = methods
    res.mean_inc = []
    res.mean_cons = []
    res.mean_sav = []

    inc_tot, n_shock, v_shock, p_series = prepare_simulation(res)

    for i_method, method in enumerate(methods):
        print('Simulations running, method ' + method)
        sfun, xvals = get_sfun_xvals(res, method)
        x_final, s_final = iterate(res.par, n_shock, v_shock, xvals, sfun,
                                   p_series, method)
        mean_inc = np.mean(inc_tot, axis=0)
        mean_cons = np.mean((x_final-s_final), axis=0)
        mean_sav = np.mean(s_final, axis=0)
        res.mean_inc.append(mean_inc)
        res.mean_cons.append(mean_cons)
        res.mean_sav.append(mean_sav)

    # Pickle and save for future
    filen = 'res_lcsim_sigma%.d_ex%.d.bin' % (res.par.sigma, res.par.exercise)
    fn = join(resdir, filen)
    pickle.dump(res, open(fn, 'wb'))
    print('End of simulation')


def main():
    methods = ['VFI']
    main_simulation(methods)


if __name__ == '__main__':
    main()
