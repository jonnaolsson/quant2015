from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from lifecycle.envir import resdir, plotdir
from common.plotting import plot_gen, plot_lorenz, get_filename, plot_cons
from common.plotting import fix_colors
import pickle
import numpy as np
import matplotlib.pyplot as plt


def get_ages():
    age_list = [15, 35, 55]
    return age_list


def plot_general(res, sfun, vfun=None, method='VFI'):
    colcyc = fix_colors()
    xx = res.par.grid.xx
    age_list = get_ages()

    sfun_values = res.par.grid.ss[sfun]
    sfun_plot = np.zeros((len(age_list), sfun_values.shape[0]))
    for it in range(len(age_list)):
        sfun_plot[it, :] = sfun_values[:, age_list[it]]

    title_string = 'Policy function ex %.d (method: ' + method + ')'
    title = title_string% (res.par.exercise)
    # text = 'r: %.6f, K: %.4f' % (res.r_aiy[it], res.k_agg_aiy[it])
    fn = get_filename(res, 'LC', 'sfun', method=method, nyy=res.par.nyy)
    plot_gen(xx, sfun_plot,
             mult_lin=True,
             title=title,
             xlab='Cash-on-hand', ylab='Savings',
             xlim=[np.min(xx), 10],
             ylim=[np.min(sfun_plot), 10],
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=age_list)

    if vfun is not None:
        vfun_plot = np.zeros((len(age_list), vfun.shape[0]))
        for it in range(len(age_list)):
            vfun_plot[it, :] = vfun[:, age_list[it]]

        title_string = 'Value function ex %.d (method: ' + method + ')'
        title = title_string % res.par.exercise
        # text = 'r: %.6f, K: %.4f' % (res.r_aiy[it], res.k_agg_aiy[it])
        fn = get_filename(res, 'LC', 'vfun', method=method, nyy=res.par.nyy)
        plot_gen(xx, vfun_plot,
                 mult_lin=True,
                 title=title,
                 xlab='Cash-on-hand', ylab='Value',
                 xlim=[np.min(xx), np.max(xx)],
                 ylim=[-32, np.max(vfun_plot)*1.05],
                 fname=join(plotdir, fn),
                 cols=colcyc,
                 llabs=age_list)


def plot_sim(res):
    colcyc = fix_colors()
    age = np.arange(20, res.par.big_t + 20)
    mean_inc_plot = np.zeros((3, res.par.big_t))
    mean_cons_plot = np.zeros((3, res.par.big_t))
    mean_sav_plot = np.zeros((3, res.par.big_t))
    for it in range(len(res.methods)):
        mean_inc_plot[it, :] = res.mean_inc[it]
        mean_cons_plot[it, :] = res.mean_cons[it]
        mean_sav_plot[it, :] = res.mean_sav[it]
    label_list = ['Income', 'Consumption', 'Savings']

    means_vfi = np.zeros((3, res.par.big_t))
    means_vfi[0, :] = mean_inc_plot[0]
    means_vfi[1, :] = mean_cons_plot[0]
    means_vfi[2, :] = mean_sav_plot[0]
    title_string = 'Age profile (means) ex %.d (method: VFI)'
    title = title_string % res.par.exercise
    fn = get_filename(res, 'LC', 'ageprof', method='VFI', nyy=res.par.nyy)
    plot_gen(age, means_vfi,
             mult_lin=True,
             title=title,
             xlab='Age', ylab='Value',
             xlim=[np.min(age), np.max(age)],
             ylim=[np.min(means_vfi), np.max(means_vfi)*1.05],
             fname=join(plotdir, fn),
             cols=colcyc,
             llabs=label_list,
             legloc='upper left')

    if len(res.methods) > 1:
        means_pfi = np.zeros((3, res.par.big_t))
        means_pfi[0, :] = mean_inc_plot[1]
        means_pfi[1, :] = mean_cons_plot[1]
        means_pfi[2, :] = mean_sav_plot[1]
        title_string = 'Age profile (means) ex %.d (method: PFI)'
        title = title_string % res.par.exercise
        fn = get_filename(res, 'LC', 'ageprof', method='PFI', nyy=res.par.nyy)
        plot_gen(age, means_pfi,
                 mult_lin=True,
                 title=title,
                 xlab='Age', ylab='Value',
                 xlim=[np.min(age), np.max(age)],
                 ylim=[0, np.max(means_pfi)*1.05],
                 fname=join(plotdir, fn),
                 cols=colcyc,
                 llabs=label_list,
                 legloc='upper left')

        means_egm = np.zeros((3, res.par.big_t))
        means_egm[0, :] = mean_inc_plot[2]
        means_egm[1, :] = mean_cons_plot[2]
        means_egm[2, :] = mean_sav_plot[2]
        title_string = 'Age profile (means) ex %.d (method: EGM)'
        title = title_string % res.par.exercise
        fn = get_filename(res, 'LC', 'ageprof', method='EGM', nyy=res.par.nyy)
        plot_gen(age, means_egm,
                 mult_lin=True,
                 title=title,
                 xlab='Age', ylab='Value',
                 xlim=[np.min(age), np.max(age)],
                 ylim=[0, np.max(means_egm)*1.05],
                 fname=join(plotdir, fn),
                 cols=colcyc,
                 llabs=label_list,
                 legloc='upper left')
