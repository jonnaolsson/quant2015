from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from lifecycle.envir import resdir, plotdir
from common.plotting import get_filename, plot_cons
from common.plotting import fix_colors
from lifecycle.plot_lc_common import plot_general, plot_sim, get_ages
import pickle
import numpy as np
import matplotlib.pyplot as plt


def load_data():
    fn = join(resdir, 'res_lcsim_sigma2_ex1.bin')
    res = pickle.load(open(fn, 'rb'))
    return res


def plot_egm(res, method='EGM'):
    cfun = res.cfun_egm
    xfun = res.xfun_egm
    colcyc = fix_colors()
    age_list = get_ages()

    cfun_plot = np.zeros((len(age_list), cfun.shape[0]))
    xfun_plot = np.zeros((len(age_list), xfun.shape[0]))
    for it in range(len(age_list)):
        cfun_plot[it, :] = cfun[:, age_list[it]]
        xfun_plot[it, :] = xfun[:, age_list[it]]

    title_string = 'Consumption function ex %.d (method: ' + method + ')'
    title = title_string% res.par.exercise
    fn = get_filename(res, 'LC', 'cfun', method=method)
    plot_cons(xfun_plot, cfun_plot,
              mult_lin=True,
              title=title,
              xlab='Cash-on-hand', ylab='Value',
              xlim=[np.min(xfun_plot), np.max(xfun_plot)],
              ylim=[np.min(cfun_plot), np.max(cfun_plot)*1.05],
              fname=join(plotdir, fn),
              cols=colcyc,
              llabs=age_list)


def plot_consumption(res, ss, age=45):
    colcyc = fix_colors()
    fn = get_filename(res, 'LC', 'cfun', age=age, nyy=res.par.nyy)
    fname = join(plotdir, fn)
    title = 'Consumption function model age %.d' % age
    plt.gca().set_color_cycle(colcyc)

    cfun_egm = res.cfun_egm[:, age]
    xfun_egm = res.xfun_egm[:, age]

    cfun_vfi = res.par.grid.xx - ss[res.sfun_vfi[:, age]]
    xfun_vfi = res.par.grid.xx

    cfun_pfi = res.par.grid.xx - ss[res.sfun_pfi[:, age]]
    xfun_pfi = res.par.grid.xx

    fig = plt.plot(xfun_egm, cfun_egm, lw=2, label='EGM')
    plt.plot(xfun_vfi, cfun_vfi, lw=2, label='VFI')
    plt.plot(xfun_pfi, cfun_pfi, lw=2, label='PFI')

    plt.legend(loc='lower right')
    plt.title(title)
    plt.xlabel('Cash on hand')
    plt.ylabel('Consumption')
    plt.xlim([0, 20])
    plt.ylim([0, 3.5])
    plt.grid(True)
    plt.savefig(fname, format='PDF')
    plt.close()


def plot_solve(res):
    plot_general(res, res.sfun_vfi, res.vfun_vfi, method='VFI')
    plot_general(res, res.sfun_pfi, method='Policy')
    plot_egm(res, method='EGM')

    plot_consumption(res, res.par.grid.ss, age=10)
    plot_consumption(res, res.par.grid.ss, age=25)
    plot_consumption(res, res.par.grid.ss, age=35)
    plot_consumption(res, res.par.grid.ss, age=45)
    plot_consumption(res, res.par.grid.ss, age=55)


def main():
    print('Start plot_res Lifecycle ex 1')
    res = load_data()

    plot_solve(res)
    plot_sim(res)
    print('Done plot_res Lifecycle ex 1')


if __name__ == '__main__':
    main()
