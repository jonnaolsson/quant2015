from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np


def solve_egm(par, ss):
    big_t = par.big_t
    sigma = par.sigma
    beta = par.beta
    r = par.r
    trend_g = par.trend_g
    n_nn = par.n_nn
    n_vv = par.n_vv
    nodes_n = par.nodes_n
    nodes_v = par.nodes_v
    weights_n = par.weights_n
    weights_v = par.weights_v

    # Initialize result matrices
    cfun = np.zeros((par.grid.nss, big_t))
    xfun = np.zeros((par.grid.nss, big_t))

    # Last period in life we consume all cash-on-hand, save nothing
    xfun[:, big_t-1] = np.linspace(0.0001, 20, par.grid.nss)
    cfun[:, big_t-1] = xfun[:, big_t-1]

    msg = 'Starting EGM... '
    print(msg)

    for t in range(par.big_t-2, -1, -1):
        # Calculate marginal value of assets at end of period
        gothic_v = np.zeros(par.grid.nss)

        # If not retired
        if t + 1 < par.t_ret:
            for innodes in range(n_nn):  # permanent shocks
                for ivnodes in range(n_vv):  # transitory shocks
                    r_idx = np.array(ss <= 0, dtype=np.uint32)
                    xprime = ss*(1+r[r_idx]) / (trend_g[t+1]*nodes_n[innodes]) \
                        + nodes_v[ivnodes]
                    cprime = np.interp(xprime,
                                       np.concatenate([[0], xfun[:, t+1]]),
                                       np.concatenate([[0], cfun[:, t+1]]))
                    gothic_v_temp = np.power(trend_g[t+1]*nodes_n[innodes], -sigma) \
                        * par.marg_util(cprime)
                    gothic_v += beta * (1+r[r_idx]) * weights_n[innodes] * \
                        weights_v[ivnodes] * gothic_v_temp

        # If retired there is no uncertainty
        else:
            r_idx = np.array(ss <= 0, dtype=np.uint32)
            xprime = ss*(1+r[r_idx]) / trend_g[t+1] + 1
            cprime = np.interp(xprime, np.concatenate([[0], xfun[:, t+1]]),
                               np.concatenate([[0], cfun[:, t+1]]))
            gothic_v = beta * (1+r[r_idx]) * np.power(trend_g[t+1], -sigma) \
                       * par.marg_util(cprime)

        # Recover optimal current consumption and cash-on-hand
        cfun[:, t] = par.marg_util_inv(gothic_v)
        xfun[:, t] = cfun[:, t] + ss

    return cfun, xfun, ss