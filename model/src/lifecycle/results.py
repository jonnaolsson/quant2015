from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'


class Results:
    def __init__(self):
        self.par = None

        self.vfun_vfi = None
        self.sfun_vfi = None

        self.sfun_pfi = None

        self.cfun_egm = None
        self.xfun_egm = None
        self.sfun_egm = None

        self.mean_inc_vfi = None
        self.mean_con_vfi = None
        self.mean_sav_vfi = None

        self.mean_inc_pfi = None
        self.mean_con_pfi = None
        self.mean_sav_pfi = None

        self.mean_inc = None
        self.mean_con = None
        self.mean_sav = None
        self.methods = None

        self.default = None






