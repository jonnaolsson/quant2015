from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np
from math import log as mlog


class ParamContainer:
    pass

par = ParamContainer()

# Choose exercise
par.exercise = 5

# Other settings
par.huge = 1e32  # what I use for very huge numbers

# Preference parameters
par.beta = 0.96     # discount factor
par.sigma = 2.0     # risk aversion
if par.sigma == 1:
    par.util = lambda c: mlog(c)
else:
    # Note - no -1 in utility function
    par.util = lambda c: (np.power(c, 1-par.sigma)) / (1-par.sigma)
par.marg_util = lambda c: np.power(c, -par.sigma)
par.marg_util_inv = lambda c: np.power(c, -1/par.sigma)

# Life time parameters
par.big_t = 65      # number of periods in life
par.t_ret = 45      # number of working years

# Interest rate
if par.exercise == 4 or par.exercise == 5:
    par.r = np.array((0.02, 0.06))
else:
    par.r = np.array((0.02, 0.02))


# Shock settings
par.var_n = 0.0106      # variance of permanent shocks
par.var_v = 0.0738      # variance of transitory shocks
par.mu_n = None         # Will be updated
par.mu_v = None         # Will be updated

# Age specific deterministic trend in income
par.trend_g = np.array((15.5273036489961, 1.13087311215982, 1.05870874450936,
                        1.05467219182954, 1.05077711505543, 1.04702205854521,
                        1.04340562226528, 1.03992646092071, 1.03658328312142,
                        1.03337485058359, 1.03029997736546, 1.02735752913683,
                        1.02454642248183, 1.02186562423414, 1.01931415084419,
                        1.01689106777780, 1.01459548894578, 1.01242657616384,
                        1.01038353864259, 1.00846563250697, 1.00667216034486,
                        1.00500247078444, 1.00345595809984, 1.00203206184495,
                        1.00073026651485, 0.999550101234814, 0.998491139476392,
                        0.997552998800479, 0.996735340627130, 0.996037870031881,
                        0.995460335568447, 0.995002529117637, 0.994664285762371,
                        0.994445483688613, 0.994346044112305, 0.994365931232058,
                        0.994505152207672, 0.994763757164433, 0.995141839223198,
                        0.995639534556274, 0.996257022469168, 0.996994525508269,
                        0.997852309594495, 0.998830684183142, 0.999930002449943,
                        0.682120000000000, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                        1, 1, 1, 1, 1, 1, 1))

# Asset grid parameters
par.grid = ParamContainer()

# Savings
par.grid.ss = None
par.grid.nss = 1600

if par.exercise == 1:
    par.grid.smin = 0
    par.grid.smax = 30
else:
    par.grid.smin = -1
    par.grid.smax = 30

# Cash-in-hand
par.grid.xx = None
par.grid.nxx = 2000
par.grid.xmin = par.grid.smin + 0.001
par.grid.xmax = 40

# Same spacing used for all grids
par.grid.spacing = 1.3

# Simulation settings
par.simulation = ParamContainer()
par.simulation.n = 1e5
par.simulation.seed = 42

par.nyy = 9            # no nodes in quadrature for income shocks
# Gauss-Hermite - will be updated in initialization
par.weights_n = None
par.weights_v = None
par.nodes_n = None
par.nodes_v = None
par.n_nn = None
par.n_vv = None
