from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np
from lifecycle.params import par


def initialize():
    # Create grid for savings
    ss = np.empty(par.grid.nss)
    ss[0] = par.grid.smin
    for i in range(ss.shape[0]):
        if i == 0:
            pass
        else:
            ss[i] = ss[i-1] + (par.grid.smax-ss[i-1]) \
                / ((par.grid.nss-i)**par.grid.spacing)

    # Create grid for cash-on-hand
    xx = np.empty((par.grid.nxx))
    xx[0] = par.grid.xmin
    for i in range(xx.shape[0]):
        if i == 0:
            pass
        else:
            xx[i] = xx[i-1] + (par.grid.xmax-xx[i-1]) \
                / ((par.grid.nxx-i)**par.grid.spacing)

    par.grid.xx = xx
    par.grid.ss = ss


    # Fix the Gauss-Hermite
    n_hermite, w_hermite = np.polynomial.hermite.hermgauss(par.nyy)
    par.weights_n = w_hermite * np.pi**(-1/2)  # weights permanent shock
    par.weights_v = w_hermite * np.pi**(-1/2)  # weights transitory shock
    par.nodes_n = np.exp(np.sqrt(2*par.var_n)*n_hermite - par.var_n/2)   # nodes permanent shock
    par.nodes_v = np.exp(np.sqrt(2*par.var_v)*n_hermite - par.var_v/2)   # nodes transitory shock
    par.n_nn = par.nyy  # number of nodes for permanent shock
    par.n_vv = par.nyy  # number of nodes for transitory shock

    # Some very basic shock calculations
    par.mu_n = -par.var_n/2
    par.mu_v = -par.var_v/2

    return ss, xx
