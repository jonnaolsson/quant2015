from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from lifecycle.envir import resdir, plotdir
from common.plotting import get_filename
from common.plotting import fix_colors
from lifecycle.plot_lc_common import plot_general, plot_sim
import pickle
import numpy as np
import matplotlib.pyplot as plt


def load_data(exercise=3):
    filen = 'res_lcsim_sigma2_ex%.d.bin' % exercise
    fn = join(resdir, filen)
    res = pickle.load(open(fn, 'rb'))
    return res


def plot_compare_means(res_a, res_b):
    colcyc = fix_colors(no=3)

    # Check that the number of nodes are the same for both graphs
    if not res_a.par.nyy == res_b.par.nyy:
        print('Note that nyy are not the same in the two runs')

    fn = get_filename(res_a, 'LC', 'compare', comp=res_b.par.exercise,
                      nyy=res_a.par.nyy)
    fname = join(plotdir, fn)
    title = 'Age profiles ex.%.d and ex.%.d' % (res_a.par.exercise,
                                                res_b.par.exercise)

    plt.gca().set_color_cycle(colcyc)

    age = np.arange(20, res_a.par.big_t + 20)
    mean_inc_plot_a = res_a.mean_inc[0]
    mean_cons_plot_a = res_a.mean_cons[0]
    mean_sav_plot_a = res_a.mean_sav[0]
    mean_inc_plot_b = res_b.mean_inc[0]
    mean_cons_plot_b = res_b.mean_cons[0]
    mean_sav_plot_b = res_b.mean_sav[0]

    y_lb = np.fmin(np.min(mean_sav_plot_a), np.min(mean_sav_plot_b))
    y_ub = np.fmax(np.max(mean_sav_plot_a), np.max(mean_sav_plot_b))

    fig = plt.plot(age, mean_inc_plot_a, lw=2, label='Income')
    plt.plot(age, mean_cons_plot_a, lw=2, label='Consumption')
    plt.plot(age, mean_sav_plot_a, lw=2, label='Savings')

    plt.plot(age, mean_inc_plot_b, '--', lw=2)
    plt.plot(age, mean_cons_plot_b, '--', lw=2)
    plt.plot(age, mean_sav_plot_b, '--', lw=2)

    text = 'Solid: ex.%1d \nDashed: ex.%1d' % (res_a.par.exercise,
                                               res_b.par.exercise)
    text2 = 'Ex.%1d: nyy=%1d, Ex.%1d: nyy=%1d' % (res_a.par.exercise,
                                                  res_a.par.nyy,
                                                  res_b.par.exercise,
                                                  res_b.par.nyy)

    plt.legend(loc='upper left')
    plt.title(title)
    plt.xlabel('Age')
    plt.ylabel('Value')
    plt.text(.82 * np.max(age), .9 * y_ub, text)
    plt.text(.5 * np.max(age), .02 * y_ub, text2)
    plt.xlim([np.min(age), np.max(age)])
    plt.ylim([y_lb*2, y_ub*1.05])
    plt.grid(True)
    plt.savefig(fname, format='PDF')
    plt.close()


def main():
    print('Start plot_res Lifecycle ex 34')
    res1 = load_data(exercise=1)

    res3 = load_data(exercise=3)
    plot_general(res3, res3.sfun_vfi, res3.vfun_vfi, method='VFI')
    plot_sim(res3)

    res4 = load_data(exercise=4)
    plot_general(res4, res4.sfun_vfi, res4.vfun_vfi, method='VFI')
    plot_sim(res4)

    plot_compare_means(res1, res3)
    plot_compare_means(res3, res4)
    plot_compare_means(res1, res4)

    msg = 'Minimum savings exercise {:.0f}: {:.3f}'
    print(msg.format(res3.par.exercise, np.min(res3.mean_sav)))
    msg = 'Minimum savings exercise {:.0f}: {:.3f}'
    print(msg.format(res4.par.exercise, np.min(res4.mean_sav)))

    print('Done ex 34')


if __name__ == '__main__':
    main()