from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from lifecycle.params import par
from lifecycle.ex134 import solve_ex
from lifecycle.ex5 import solve_ex5


def main():
    print('Life-cycle models started\n')
    msg = 'Solving exercise {:.0f} (quadrature nodes: {:.0f})'
    print(msg.format(par.exercise, par.nyy))

    if par.exercise == 1:
        methods = ['VFI', 'PFI', 'EGM']
        solve_ex(par.exercise, methods)

    elif par.exercise == 3:
        methods = ['VFI']
        solve_ex(par.exercise, methods)

    elif par.exercise == 4:
        methods = ['VFI']
        solve_ex(par.exercise, methods)

    elif par.exercise == 5:
        solve_ex5()

    else:
        print('Unknown exercise, do not know what to do... ')

if __name__ == '__main__':
    main()