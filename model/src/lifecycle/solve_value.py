from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np
from time import perf_counter


def solve_vfi(par, ss, xx):
    big_t = par.big_t
    sigma = par.sigma
    beta = par.beta
    r = par.r
    trend_g = par.trend_g
    n_nn = par.n_nn
    n_vv = par.n_vv
    nodes_n = par.nodes_n
    nodes_v = par.nodes_v
    weights_n = par.weights_n
    weights_v = par.weights_v

    # Initialize result matrices
    vfun = np.empty((par.grid.nxx, big_t))
    sfun = np.empty((par.grid.nxx, big_t), dtype=np.uint32)

    # Last period in life we consume all cash-on-hand, save nothing
    # Note that we cannot allow negative consumption (change necessary in ex 3)
    # If negative, we set it to an extremely small number
    cons = np.fmax(xx, 1/par.huge)
    vfun[:, big_t-1] = par.util(cons)
    sfun[:, big_t-1] = 0

    msg = 'Starting VFI... '
    print(msg)

    # Choose the right interest rate depending on borrow or save
    r_eff = np.where(ss < 0, par.r[1], par.r[0])

    # pre-compute expectation weights
    inn = np.arange(n_nn).reshape((-1, 1))
    ivv = np.arange(n_vv).reshape((1,-1))
    wgt = np.power(nodes_n[inn], 1-sigma) * weights_n[inn] * weights_v[ivv]

    for t in range(par.big_t-2, -1, -1):

        # First calculate expected value tomorrow
        vprime = np.zeros((ss.shape[0]))

        # Expected value if not retired yet
        if t + 1 < par.t_ret:
            for innodes in range(n_nn):  # permanent shocks
                for ivnodes in range(n_vv):  # transitory shocks
                    xprime = ss*(1+r_eff) / (trend_g[t+1]*nodes_n[innodes]) \
                        + nodes_v[ivnodes]
                    # Check if xprime is valid (or will give neg. cons)
                    # otherwise set an extremely large negative number
                    idx_xvalid = xprime >= par.grid.smin
                    v_temp = np.empty_like(vprime)
                    v_temp[idx_xvalid] = np.interp(xprime[idx_xvalid], xx, vfun[:, t+1])
                    v_temp[~idx_xvalid] = -par.huge
                    vprime += wgt[innodes, ivnodes]*v_temp

        # Expected value if retired - no uncertainty
        else:
            xprime = ss*(1+r_eff) / trend_g[t+1] + 1
            vprime = np.interp(xprime, xx, vfun[:, t+1])

        # Premultiply by...
        vprime *= trend_g[t+1]**(1-sigma)

        # Then pick the max
        for i_xx_from, x_from in enumerate(xx):
            idx = ss <= x_from
            c = x_from - ss[idx]
            v = par.util(c) + beta*vprime[idx]
            vmax = np.max(v)
            i_opt = np.argmax(v)
            vfun[i_xx_from, t] = vmax
            sfun[i_xx_from, t] = i_opt

    return vfun, sfun


def solve_vfi_nodefault(par, ss, xx, vfun_default, xx_default):
    big_t = par.big_t
    sigma = par.sigma
    beta = par.beta
    r = par.r
    trend_g = par.trend_g
    n_nn = par.n_nn
    n_vv = par.n_vv
    nodes_n = par.nodes_n
    nodes_v = par.nodes_v
    weights_n = par.weights_n
    weights_v = par.weights_v

    # Initialize result matrices
    vfun = np.zeros((par.grid.nxx, big_t))
    sfun = np.zeros((par.grid.nxx, big_t), dtype=np.uint32)

    # Last period in life we consume all cash-on-hand, save nothing
    cons = np.fmax(xx, 1/par.huge)
    vfun[:, big_t-1] = par.util(cons)
    sfun[:, big_t-1] = 0

    t_start = perf_counter()
    msg = 'Starting VFI... '
    print(msg)

    r_eff = np.where(ss < 0, par.r[1], par.r[0])

    # pre-compute expectation weights
    inn = np.arange(n_nn).reshape((-1, 1))
    ivv = np.arange(n_vv).reshape((1,-1))
    wgt = np.power(nodes_n[inn], 1-sigma) * weights_n[inn] * weights_v[ivv]

    for t in range(par.big_t-2, -1, -1):

        # First calculate expected value tomorrow
        vprime = np.zeros((ss.shape[0]))
        vprime_def = np.zeros((ss.shape[0]))
        vprime_actual = np.zeros((ss.shape[0]))

        # Expected value if not retired yet
        if t + 1 < par.t_ret:
            for innodes in range(n_nn):  # permanent shocks
                for ivnodes in range(n_vv):  # transitory shocks
                    # First calculate the case of no-default
                    xprime = ss*(1+r_eff) / (trend_g[t+1]*nodes_n[innodes]) \
                        + nodes_v[ivnodes]
                    # Check if xprime is valid (or will give neg. cons)
                    # otherwise set an extremely large negative number
                    idx_xvalid = xprime >= par.grid.smin
                    v_temp = np.empty_like(vprime)
                    v_temp[idx_xvalid] = np.interp(xprime[idx_xvalid], xx, vfun[:, t+1])
                    v_temp[~idx_xvalid] = -par.huge
                    # Calculate the case of default
                    v_temp_default = np.interp(nodes_v[ivnodes], xx_default,
                                               vfun_default[:, t+1])
                    # Then pick the max of this and the value if we default
                    v_temp_actual = np.fmax(v_temp, v_temp_default)
                    vprime_actual += wgt[innodes, ivnodes] * v_temp_actual

        # Expected value if retired - no uncertainty
        else:
            # Calculate the case of no-default
            xprime = ss*(1+r_eff) / trend_g[t+1] + 1
            vprime = np.interp(xprime, xx, vfun[:, t+1])
            # Calculate the case of default
            vprime_def = np.interp(1.0, xx_default, vfun_default[:, t+1])
            # Then pick the max of this and the value if we default next period
            vprime_actual = np.fmax(vprime, vprime_def)

        # Premultiply by...
        vprime_actual *= trend_g[t+1]**(1-sigma)

        # Then pick the max
        for i_xx_from, x_from in enumerate(xx):
            idx = ss <= x_from
            c = x_from - ss[idx]
            v = par.util(c) + beta*vprime_actual[idx]
            vmax = np.max(v)
            i_opt = np.argmax(v)
            vfun[i_xx_from, t] = vmax
            sfun[i_xx_from, t] = i_opt

    t_end = perf_counter()
    msg = 'VFI done in {:.1f} sec\n'
    print(msg.format(t_end-t_start))

    return vfun, sfun

