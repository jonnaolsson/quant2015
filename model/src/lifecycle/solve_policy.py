from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

import numpy as np


def solve_policy(par, ss, xx):
    big_t = par.big_t
    sigma = par.sigma
    beta = par.beta
    r = par.r
    trend_g = par.trend_g
    n_nn = par.n_nn
    n_vv = par.n_vv
    nodes_n = par.nodes_n
    nodes_v = par.nodes_v
    weights_n = par.weights_n
    weights_v = par.weights_v

    # Initialize result matrix
    sfun = np.zeros((par.grid.nxx, big_t), dtype=np.uint32)

    # Last period in life we consume all cash-on-hand, save nothing
    sfun[:, big_t-1] = 0

    msg = 'Starting PFI... '
    print(msg)

    for t in range(par.big_t-2, -1, -1):

        # First calculate expected marginal value tomorrow
        mu_prime = np.zeros((ss.shape[0]))

        # Expected value if not retired yet
        if t + 1 < par.t_ret:
            for innodes in range(n_nn):  # permanent shocks
                for ivnodes in range(n_vv):  # transitory shocks
                    r_idx = np.array(ss <= 0, dtype=np.uint32)
                    xprime = ss*(1+r[r_idx]) / (trend_g[t+1]*nodes_n[innodes]) \
                        + nodes_v[ivnodes]
                    cprime = xprime - np.interp(xprime, xx, ss[sfun[:, t+1]])
                    mu_temp = np.power(trend_g[t+1]*nodes_n[innodes], -sigma) \
                        * par.marg_util(cprime)
                    mu_prime += beta*(1+r[r_idx])*weights_n[innodes] * \
                        weights_v[ivnodes] * mu_temp

        # Expected value if retired
        else:
            r_idx = np.array(ss <= 0, dtype=np.uint32)
            xprime = ss*(1+r[r_idx]) / trend_g[t+1] + 1
            cprime = xprime - np.interp(xprime, xx, ss[sfun[:, t+1]])
            mu_prime = beta*(1+r[r_idx])*np.power(trend_g[t+1], -sigma) * \
                       par.marg_util(cprime)
        # Then pick the optimum (smallest EE error)
        for i_xx_from, x_from in enumerate(xx):
            # Only consider those we can afford
            idx = ss <= x_from
            mu = par.marg_util(x_from - ss[idx])
            i_opt = np.argmin(np.abs(mu_prime[idx] - mu))
            sfun[i_xx_from, t] = i_opt

    return sfun