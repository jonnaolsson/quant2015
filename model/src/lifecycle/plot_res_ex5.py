from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
from lifecycle.envir import plotdir
from common.plotting import plot_gen, get_filename
from common.plotting import fix_colors
from lifecycle.plot_res_ex34 import load_data, plot_compare_means, plot_general
import numpy as np


def plot_default(res):
    colcyc = fix_colors(no=3)
    fn = get_filename(res, 'LC', 'default')
    title = 'Default ratio ex.%.d' % res.par.exercise

    age = np.arange(20, res.par.big_t + 20)

    plot_gen(age, res.default*100,
             mult_lin=False,
             title=title,
             xlab='Age', ylab='Percent',
             xlim=[np.min(age), np.max(age)],
             ylim=[0, 100],
             fname=join(plotdir, fn),
             cols=colcyc)


def plot_cons_diff(res_a, res_b):
    colcyc = fix_colors(no=3)
    title = 'Percentage increase in consumption from ex.%.d to ex.%.d' % \
            (res_a.par.exercise, res_b.par.exercise)
    fn = get_filename(res_b, 'LC', 'cons_inc')

    cons_a = res_a.mean_cons[0]
    cons_b = res_b.mean_cons[0]

    perc_diff = (cons_b - cons_a)/cons_b * 100
    age = np.arange(20, res_a.par.big_t + 20)

    plot_gen(age, perc_diff,
             mult_lin=False,
             title=title,
             xlab='Age', ylab='Percent',
             xlim=[np.min(age), np.max(age)],
             ylim=[0, 15],
             fname=join(plotdir, fn),
             cols=colcyc)


def main():
    print('Start plot_res Lifecycle ex 45')

    res1 = load_data(exercise=1)
    res4 = load_data(exercise=4)
    res5 = load_data(exercise=5)

    plot_compare_means(res1, res5)
    plot_compare_means(res4, res5)

    plot_default(res5)
    plot_general(res5, res5.sfun_vfi, res5.vfun_vfi, method='VFI')
    plot_cons_diff(res4, res5)

    msg = 'Minimum savings exercise {:.0f}: {:.3f}'
    print(msg.format(res1.par.exercise, np.min(res1.mean_sav)))
    msg = 'Minimum savings exercise {:.0f}: {:.3f}'
    print(msg.format(res4.par.exercise, np.min(res4.mean_sav)))
    msg = 'Minimum savings exercise {:.0f}: {:.3f}'
    print(msg.format(res5.par.exercise, np.min(res5.mean_sav)))

    print('Done ex 45')


if __name__ == '__main__':
    main()