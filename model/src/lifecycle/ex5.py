from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
import pickle
import numpy as np
from lifecycle.envir import resdir
from lifecycle.simulate import prepare_simulation, iterate5
from lifecycle.params import par
from lifecycle.solve_value import solve_vfi_nodefault
from lifecycle.initialize import initialize
from lifecycle.results import Results


def solve_ex5():
    # First unpickle the results from exercise 1
    file = 'res_lc_sigma2_ex1.bin'
    fn = join(resdir, file)
    res_ex1 = pickle.load(open(fn, 'rb'))

    # Then initialize this exercise
    ss, xx = initialize()
    res = Results()
    res.par = par

    # Results from exercise 1 will be used here
    vfun_default = res_ex1.vfun_vfi
    sfun_default = res_ex1.sfun_vfi
    xx_default = res_ex1.par.grid.xx
    ss_default = res_ex1.par.grid.ss

    # Find value function and savings function for no default case
    vfun_nodefault, sfun_nodefault = \
        solve_vfi_nodefault(par, ss, xx, vfun_default, xx_default)

    # Prepare simulation values
    inc_tot, n_shock, v_shock, p_series = prepare_simulation(res)

    # Iterate the panel of many households
    norm_x, norm_s, default = iterate5(par, n_shock, v_shock, xx_default,
                                       xx, vfun_default, vfun_nodefault,
                                       sfun_default, sfun_nodefault,
                                       ss_default, ss)

    # De-normalize
    x_final = norm_x * p_series
    s_final = norm_s * p_series

    mean_inc = np.mean(inc_tot, axis=0)
    mean_cons = np.mean((x_final-s_final), axis=0)
    mean_sav = np.mean(s_final, axis=0)

    res.mean_inc = []
    res.mean_cons = []
    res.mean_sav = []
    res.mean_inc.append(mean_inc)
    res.mean_cons.append(mean_cons)
    res.mean_sav.append(mean_sav)
    res.default = np.mean(default, axis=0)

    res.vfun_vfi = vfun_nodefault
    res.sfun_vfi = sfun_nodefault

    # Remove the util functions since you cannot pickle lambda functions
    par.util = None
    par.marg_util = None
    par.marg_util_inv = None

    # Pickle and save for future
    filen = 'res_lcsim_sigma%.d_ex%.d.bin' % (par.sigma, par.exercise)
    fn = join(resdir, filen)
    pickle.dump(res, open(fn, 'wb'))


