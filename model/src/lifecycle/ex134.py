from __future__ import print_function, division, absolute_import

__author__ = 'Jonna Olsson'

from os.path import join
import pickle
from lifecycle.envir import resdir
from lifecycle.params import par
from lifecycle.initialize import initialize
from lifecycle.solve_value import solve_vfi
from lifecycle.solve_policy import solve_policy
from lifecycle.solve_egm import solve_egm
from lifecycle.results import Results
from lifecycle.simulate import main_simulation
from time import perf_counter


def solve_ex(exercise, methods):
    ss, xx = initialize()
    res = Results()
    res.par = par

    # Solve by value function iteration
    t_start_vfi = perf_counter()
    vfun_vfi, sfun_vfi = solve_vfi(par, ss, xx)
    t_end_vfi = perf_counter()
    res.vfun_vfi = vfun_vfi
    res.sfun_vfi = sfun_vfi
    msg = 'VFI done in {:.1f} sec\n'
    print(msg.format(t_end_vfi-t_start_vfi))

    if exercise == 1:
        # Solve by policy function iteration
        t_start_pfi = perf_counter()
        sfun_policy = solve_policy(par, ss, xx)
        t_end_pfi = perf_counter()
        res.sfun_pfi = sfun_policy
        msg = 'PFI done in {:.1f} sec\n'
        print(msg.format(t_end_pfi-t_start_pfi))

        # Solve by EGM
        t_start_egm = perf_counter()
        cfun_egm, xfun_egm, sfun_egm = solve_egm(par, ss)
        t_end_egm = perf_counter()
        res.cfun_egm = cfun_egm
        res.xfun_egm = xfun_egm
        res.sfun_egm = sfun_egm
        msg = 'EGM done in {:.1f} sec\n'
        print(msg.format(t_end_egm-t_start_egm))

    # Remove the util functions since you cannot pickle lambda functions
    par.util = None
    par.marg_util = None
    par.marg_util_inv = None

    # Pickle and save for future
    filen = 'res_lc_sigma%.d_ex%.d.bin' % (par.sigma, par.exercise)
    fn = join(resdir, filen)
    pickle.dump(res, open(fn, 'wb'))

    # Simulate the profiles
    print('Run the simulations...')
    main_simulation(methods, par.sigma, par.exercise)