\documentclass{article}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage{float}
%\usepackage{undertilde}
\usepackage{mathrsfs}
\usepackage{epstopdf}
\usepackage{listings}
\usepackage{tabu}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{booktabs,caption,fixltx2e}
\usepackage[flushleft]{threeparttable}
%%\usepackage{courier}
\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
\usepackage{booktabs}
\usepackage{fancyhdr}
\usepackage[colorlinks=true, hidelinks]{hyperref}
\usepackage[tmargin=1.1in,bmargin=1.1in,lmargin=1.45in,rmargin=1.45in]{geometry}

%% User defined commands
\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
	% Integrals
\newcommand*\diff{\mathop{}\!\mathrm{d}}
	% Symbols
\newcommand{\RR}{\mathbb{R}}
\newcommand{\LL}{\mathscr{L}}
	% Operators
\newcommand{\Max}[2]{\max_{#1} \left\{ #2 \right\} } % Maximum
\newcommand{\Min}[2]{\min_{#1} \left\{ #2 \right\} } % Minimum
\newcommand{\Argmax}[2]{\argmax_{#1} \left\{ #2 \right\} }
\newcommand{\Argmin}[2]{\argmin_{#1} \left\{ #2 \right\} }
\newcommand{\Ln}[1]{\ln{ \left( #1 \right)} }
\newcommand{\Sumt}{\sum_{t=0}^\infty }
	% Statistics
\newcommand{\E}[1]{\text{E} \left[ #1 \right]}
\newcommand{\Var}[1]{\text{Var} \left( #1 \right)}
\newcommand{\Cov}[1]{\text{Cov} \left( #1 \right)}
\newcommand{\Y}{\utilde{Y}}
\newcommand{\X}{\utilde{X}}
\newcommand{\Z}{\utilde{Z}}
\newcommand{\e}{\utilde{e}}
\newcommand{\ML}[1]{\hat{1}_{\text{ML}}}
	% Parantheses
		% Normal
\newcommand{\Le}{\left(}
\newcommand{\Ri}{\right)}
\newcommand{\Lep}{\left(}
\newcommand{\Rip}{\right)}
\newcommand{\lep}{\left(}
\newcommand{\rip}{\right)}
		% Curly
\newcommand{\Lec}{\left\{}
\newcommand{\Ric}{\right\}}
\newcommand{\lec}{\left\{}
\newcommand{\ric}{\right\}}
		% Hard
\newcommand{\Leh}{\left[}
\newcommand{\Rih}{\right]}
\newcommand{\leh}{\left[}
\newcommand{\rih}{\right]}

\definecolor{steelblue}{RGB}{64,134,170}

\begin{document}

\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE,RO]{\thepage}
\lhead{Quantitative Macro Methods 2015, Jonna Olsson}
\fancyfoot{}

\title{Quantitative Macro Methods Research Projects \\ \large Problem Set 1}
\author{Jonna Olsson\thanks{jonna.olsson@ne.su.se}}
\maketitle

\section{Introduction}
In this document the findings from the research project are documented. The algorithms used to solve the models are briefly described, all code used for the project can be found at the following web page: \url{https://bitbucket.org/jonnaolsson/quant2015/overview}. Hopefully it should be clear where you can find what in the code. The code can also be downloaded fairly easily if you want to run anything, just let me know if there is any problem. 

\section{Huggett (1993)}
There is a continuum of measure 1 of households. A household's position in each time period is described by an individual state vector $x \in \mathcal{X}$. $x = (a, m, \ell)$ indicates an agent's assets, money holdings and labor endowment. The individual state space is $\mathcal{X} = \mathcal{A} \times \mathcal{M} \times \mathcal{L}$, where $\mathcal{A} = [\underline{a}, \overline{a}]$, $\mathcal{M} = [0, \overline{m}]$ and
\[
	\mathcal{L} = \{0, 0.75, 1, 1.25, 10 \}
\]
The idiosyncratic labor endowment $\ell$ follows a given Markov process. Let $\Omega$ be the probability measure on $\mathcal{X}$. 

The households solve the following recursive problem: 
\begin{equation} \label{eq:Huggett_HHP}
	v(a, m, \ell) = \max_{c, a', m'} \Big\{ u(c) + \beta E \leh v(a', m', \ell') \rih \Big\} 
\end{equation}	
\begin{align*}
	\text{s.t. } c + qa' + pm' &= \ell(1- \tau) + b(\ell) + a + pm\\
	a' &\geq \underline{a} \\
	m' &\geq 0
\end{align*}
The per-period utility function is given by: 
\[
	u(c) = \frac{c_t^{1-\sigma}-1}{1-\sigma}
\]

We assume the agents can't borrow, i.e. $\underline{a} = 0$. The unemployment benefit $b=0.25$ is given to the agents with the lowest labor endowment and financed through a proportional labor income tax $\tau$. 

\subsection{Parametrization}
I parametrize the model according to table \ref{tab:huggett_calibration} below.  
\begin{table}[H]
\begin{center}
\begin{tabular}{ clr } 
\toprule
\textbf{Parameter} & \textbf{Description} & \textbf{Value} \\ 
\midrule
$\beta$ & Discount factor & 0.96  \\ 
$\sigma$ & Relative risk aversion & 1.00  \\ 
$\tau$ & Tax rate (\%) & $0.34$  \\ 
$\underline{a}$ & Borrowing limit & $0$  \\ 
\bottomrule
\end{tabular}
\caption {Calibration parameters} \label{tab:huggett_calibration} 
\end{center}
\end{table}

\subsection{Autarky equilibrium}
First we solve for the autarky equilibrium where there is no money. Hence, we set $m=0$ for all households and reduce the individual state space to $\mathcal{X}= \mathcal{A} \times \mathcal{L}$. An autarky equilibrium in this model is given by a price $q$, a value function $v(a, \ell)$, decision functions $h(a, \ell), g(a, \ell)$, and a distribution $\Omega$ such that: 
\begin{description}
	\item[Households' problem:] The value function $v(a, \ell)$ and the decision functions $c = h(a, \ell)$ and $a'= g(a, \ell)$ solve the households' problem given by equation \eqref{eq:Huggett_HHP}. 
	\item[Government budget balance:] The unemployment benefits paid out by the government must equal taxes raised: 
	\[ \int_{\mathcal{X}}b(\ell)\diff \Omega 
	= \tau \int_{\mathcal{X}}\ell\diff \Omega 
	\]
	\item[Market clearing:] There must be market clearing on the asset market and the goods market:
  	\begin{align*}
	\int_{\mathcal{X}}g(a, \ell) \diff \Omega  &= 0 \\
	\int_{\mathcal{X}}h(a, \ell) \diff \Omega  &= \int_{\mathcal{X}}\ell \diff \Omega  
	\end{align*}
	\item[Stationary distribution:] $\Omega$ is a stationary probability measure. 
\end{description}

\subsubsection{Solving for the autarky equilibrium}
In this setting, when we have no borrowing, the autarky equilibrium can be solved for analytically. We must find a $q$ that is high enough so that the household with the highest willingness to save still does not want to save. The household's Euler Equation reads:
\begin{equation}
	u'(c) = \beta \frac{1}{q}E\leh u'(c') \rih
\end{equation}
Hence, solving for the lowest $q$ such that the household with the highest labor endowment (who should be the one with the highest incentive to save) still does not want to save, i.e. when $c=(1-\tau)l$ gives us: 
\begin{equation}
	q = \beta \frac{\sum_{i\in \mathcal{L}}\pi(\ell'=\ell_i|l=\ell_5)u'((1-\tau)\ell_i)}{u'((1-\tau)\ell_5)} \approx 5.47
\end{equation}
After finding this lower bound of $q$ I solve the model numerically to get the value functions and policy functions. 

\subsection{Stationary fiat money equilibrium}
A stationary equilibrium with valued fiat money in this model is given by a set of prices $\{p, q\}$, a value function $v(a, m, \ell)$, decision functions $h(a, m, \ell)$, $g(a, m, \ell)$ and $m(a, m, \ell)$, and a distribution $\Omega$ such that: 
\begin{description}
	\item[Households' problem:] The value function $v(a, m, \ell)$ and the decision functions $c = h(a, m, \ell)$, $a'= g(a, m, \ell)$ and $m'= m(a, m, \ell)$ solve the households' problem given by equation \eqref{eq:Huggett_HHP}. 
	\item[Government budget balance:] The unemployment benefits paid out by the government must equal the taxes raised: 
	\[ \int_{\mathcal{X}}b(\ell)\diff \Omega 
	= \tau \int_{\mathcal{X}}\ell\diff \Omega 
	\]
	\item[Market clearing:] There must be market clearing on the asset market, the money market and the goods market:
\begin{align*}
	\int_{\mathcal{X}}g(a, m, \ell) \diff \Omega  &= 0 \\
	\int_{\mathcal{X}}m(a, m, \ell) \diff \Omega  &= \overline{M}  \\
	\int_{\mathcal{X}}h(a, m, \ell) \diff \Omega  &= \int_{\mathcal{X}}\ell \diff \Omega  
	\end{align*}
	\item[Stationary distribution:] $\Omega$ is a stationary probability measure. 
\end{description}


\subsubsection{Solving for the fiat money equilibrium}
We are looking for the stationary equilibrium, i.e. when $p_t = p_{t+1}$. Setting up the problem and taking the FOCs with respect to $a'$ and $m'$ gives us the two following equations:  
\begin{align*}
	u'(c) &= \beta \frac{1}{q}E\leh u'(c') \rih \\
	u'(c) &= \beta \frac{p_{t+1}}{p_t}E\leh u'(c') \rih
\end{align*}
We realize that we must have $q=1$ in a stationary equilibrium with valued fiat money, otherwise we would have arbitrage. Since we have no borrowing in this model, all savings is done via money. I can therefore in the model still consider only real savings, $S$. I should always have $S = \overline{M}/p$.  Hence, I set $q=1$ and solve the model numerically to get the value functions and policy functions. 


\subsection{Comparing the two economies}
Table \ref{tab:huggett_comp2ec} shows the results from the two economies. The economy with fiat money by definition has higher wealth inequality (since here we actually have some wealth), but at the same time the weighted welfare is higher in this economy. The households appreciate the existence of a savings vehicle. Hence, in a utilitarian welfare sense, a household would prefer to be born into the world with fiat money. Figure \ref{fig:huggett_lorenz} shows the corresponding Lorenz curves. Value functions and policy functions for the two economies can be found in the appendix. 

\begin{center}
\setlength{\tabcolsep}{6pt}
\begin{table}[H]\centering 
\ra{1.3} 
\begin{tabular}{@{}lrr@{}}\toprule
& \textbf{Autarky} & \textbf{Fiat money} \\ 
\midrule
%$dir=1$\\
Real interest rate & -82\% & 0.00 \\
Asset price ($q$) & 5.47 & 1.00 \\
\midrule
Gini Wealth & N/A & 0.837 \\
Gini Income & 0.318 & 0.318 \\
\midrule
Weighted welfare & 3.058 & 6.490 \\
\bottomrule
\end{tabular} 
\caption{Results from the autarky economy and the fiat money economy. Income is only labor income (pre taxes and transfers), no-one has any capital income since the real interest rate is $0$.} 
\label{tab:huggett_comp2ec}
\end{table}
\end{center}

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{../plots/huggett/huggett_aut_lorenz_sigma1.pdf}
                \caption{Economy 1 - Autarky}
                \label{fig:huggett_aut_lorenz}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{../plots/huggett/huggett_meq_lorenz_sigma1.pdf}
                \caption{Economy 2 - Money equilibrium}
        \end{subfigure}
        \caption{Lorenz curves for the two economies}\label{fig:huggett_lorenz}
\end{figure}


\subsection{Transition paths}
Now we examine the transition path from autarky to the money equilibrium. We examine two scenarios: 
\begin{itemize}
	\item[a)] All households receive one unit of money
	\item[b)] The total money supply is given to the unemployed
\end{itemize}
The resulting transition paths for the price of assets, $q$ (the inverse of the gross interest rate), and for the price level are shown in figure \ref{fig:huggett_trans_path}. When the money is initially given to everyone, the interest rate will be positive (i.e. $q<1$) and we will have a deflationary path. The two largest groups, group 3 and 4, are given relatively more money than they want to hold in equilibrium, i.e. demand is relatively low. This drives down the price of money in terms of goods, in other words, drive up the price of goods in terms of money.  

On the other hand, when we give money only to the unemployed, we will have the reversed situation: the interest rate will be negative and we will have an inflationary path. The reason is that everyone except the unemployed has no money but a strong incentive to save, i.e. to buy money. The demand for money is high, so for the market to clear the interest rate must be negative (i.e. we must have a $q>1$). 

The unemployed are indeed better off if they receive all money, but since they only constitute 2\% of the population, their gain is more than outweighed by the loss of the rest. The reason why the rest of the population experience a welfare loss is that everyone saves in this economy. A positive interest rate is therefore preferable and will give higher total welfare. 

The resulting values for the economy as a whole and for each labor group are shown in table \ref{tab:huggett_trans_welfare}. The values are calculated as the weighted sum of the discounted infinite stream of utility. In practice, for the transition paths I use the value functions for the first period to properly take into account the transition period dynamics. 

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{../plots/huggett/huggett_m_q_path_sigma1.pdf}
                \caption{Transition path for $q$}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{../plots/huggett/huggett_m_p_path_sigma1.pdf}
                \caption{Transition path for $p$}
        \end{subfigure}
        \caption{Transition paths for $q$ and $p$}\label{fig:huggett_trans_path}
\end{figure}


\begin{center}
\setlength{\tabcolsep}{4pt}
\begin{table}[H]\centering
\ra{1.3}
\begin{tabular}{@{}lrcrrcrr@{}}\toprule
& \multicolumn{1}{c}{$\textbf{Welfare}$} & \phantom{}& \multicolumn{5}{c}{$\textbf{W per labor status}$} \\ 
\cmidrule{2-2} \cmidrule{4-8} 
& (weighted)  && $\ell_1$ & $\ell_2$ & $\ell_3$ & $\ell_4$ & $\ell_5$ \\ 
\midrule
%$dir=1$\\
Autarky 						& 3.1 && -0.7 & 1.2 & 1.9 & 4.6 & 7.0 \\
Stationary money equilibrium 	& 6.5 &&  2.6 & 4.4 & 4.7 & 8.2 & 15.4 \\
a) everyone gets money 			& 6.9 &&  4.0 & 4.8 & 5.1 & 8.6 & 14.2 \\
b) money given to the unemp. 	& 5.9 && 32.0 & 2.9 & 3.4 & 7.3 & 13.4 \\
\midrule
Share of population (\%) 		&  && 2.0 & 12.0 & 40.7 & 41.3 & 4.1 \\
\bottomrule
\end{tabular}
\caption{Welfare analysis for the different scenarios.} \label{tab:huggett_trans_welfare}
\end{table}
\end{center}

\subsection{Notes on implementation}
In this section, the algorithm used is briefly presented (since it follows very closely the algorithm discussed in class). 
\subsubsection{Algorithm used}
\begin{description}
	\item[Create grid:] For the model I use the following number of grid points: $n_A=6000$. Note that also when solving for the economies where we have money, I use a grid for real savings, not for money holdings. Hence, as soon as I want to check money holdings in nominal terms, I have to divide by $p$. 
	\item[Solve for money equilibrium: ] I first solve for the money equilibrium.
	\item[Guess transition path: ] I make an initial guess for transition path of $p^{-1}$ from time $t=0$ to $t=T$. \footnote{There is no particular reason why I guess for the inverse of $p$ (except that I started coding it like that, and it does not really matter). In the code I transform this price path to the corresponding $q$ for each time period anyway.} 
	\item[Find policy functions:] I solve the household problem backwards, starting in time $t=T-1$, using the value functions from the money equilibrium as next-period value functions. The results depend on the price path I have guessed. This gives me a sequence of policy functions. 
	\item[Iterate forward:] Starting in time 0 with the resulting distribution of real asset holdings I get from the money drop method I am analyzing, which I call $\Omega_{drop}$, I iterate forward to get $\Omega_1, \Omega_2, ..., \Omega_T$, using the policy functions from the previous step and the Markov process for labor. I can now for each period calculate which $p$ I should have had to clear the market for money. 
	\item[Compare price paths: ] I can thereafter compare my guess for the price path with the resulting price path that would have cleared the market, and if they are not close enough, I adjust my guess and go back to find new policy functions. 
	\item[Check distribution: ] When the price path has converged, I check that the resulting distribution in time $T$ is close to the distribution in the money equilibrium. 
\end{description}

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=0.8\textwidth]{../plots/huggett/huggett_trans_distbin_sigma1.pdf} 
	\caption{Distribution of real savings for the money equilibrium and at the end of the transition path for the two scenarios. There is no discernible difference between the end of the paths and the equilibrium distribution. A check shows that the resulting difference is at the $10^{-4}$ level. }
	\end{center}
\end{figure}


\section{Krusell and Smith (1998)}
We now consider the same economy as in the previous question, except that we introduce production. The production function in the economy is given by:
\[
	F(K, L, A) = AK^{\alpha} L^{1-\alpha}
\]
where $K$ is aggregate capital, $L$ is aggregate labor, and $A$ is the TFP shock. $A \in \{0.85, 1.0, 1.05\}$, and we are given the transition matrix for $A$. 

An household's position in each time period is described by an individual state vector $x \in \mathcal{X}$. $x = (a, \ell)$ indicates a household's assets and labor endowment (which follows the same Markov process as in the previous question). The individual state space is $\mathcal{X} = \mathcal{A} \times \mathcal{L}$, with $\mathcal{A}$ and $\mathcal{L}$ defined as in previous exercise. Let $\Omega$ be the probability measure on $\mathcal{X}$. Hence, the household is solving the following recursive problem:
\begin{equation} \label{eq:KS_HHP}
	v(a, l; A, K) = \max_{c, a'} \Big\{ u(c) + \beta E \leh v(a', \ell'; A', K') \rih \Big\} 
\end{equation}	
\begin{align*}
	\text{s.t. } c + a' &= w \ell(1- \tau) + b(\ell) + (1+r)a \\
	a' &\geq \underline{a} \\
	\log K' &= \gamma_0(A) + \gamma_1(A) \log K \\
	A' &= G(A)
\end{align*}
where the per-period utility function $u(c)$ is the same as in previous question, $w$ is the wage, and $r$ is the real interest rate. 

We assume the agents can't borrow, i.e. $\underline{a} = 0$. The unemployment benefit $b$ is given to the agents with the lowest labor endowment and financed through a proportional labor income tax $\tau$. 

\subsection{Equilibrium}
An equilibrium in this model is given by a set of prices $\{w, r\}$, a value function $v(a, l; A, K)$, decision functions $h(a, l; A, K), g(a, l; A, K)$, a set of coefficients $\{\gamma_0(A), \gamma_1(A)\}$, and a tax rate $\tau$ such that: 
\begin{description}
	\item[Households' problem:] The value function $v(a, \ell; A, K)$ and the decision functions $c = h(a, \ell; A, K), a'= g(a, \ell; A, K)$ solve the households' problem given by equation \eqref{eq:KS_HHP}. 
	\item[Firm's problem:] Firms optimize, i.e. we have 
	\[r = F_1(K,L,A), \quad w = F_2(K,L,A) \qquad \forall A,K\] 
	\item[Government budget balance:] The unemployment benefits paid out by the government must equal taxes raised: 
	\[ \int_{\mathcal{X}}b(\ell)\diff \Omega 
	= \tau \int_{\mathcal{X}}w \ell\diff \Omega \qquad \forall A,K
	\]
	\item[Law of motion:] The households' perceived law of motion for next-period aggregate capital holds:
	\[
		\int_{\mathcal{X}}g(a,\ell;A,K) \diff \Omega 
		= \exp(\gamma_0(A) + \gamma_1(A) \log K) \qquad \forall A,K
	\]
	\item[Resource constraint:] The aggregate resource constraint holds: 
	\[\int_{\mathcal{X}}h(a,\ell;A,K) \diff \Omega 
	+ \int_{\mathcal{X}}g(a,\ell;A,K) \diff \Omega 
	= F(K, L, A) + \int_{\mathcal{X}}a \diff \Omega \qquad \forall A,K
	\]
\end{description}


\subsection{The three economies}
The model is solved for the following three economies:
\begin{description}
	\item[\textbf{E1}] The government provides an unemployment benefit $b=0.25 w$ for the lowest labor endowment realization. 
	\item[\textbf{E2}] The unemployment benefit is increased to $b=0.7 w$.
	\item[\textbf{E3}] Keeping the high unemployment benefit, assume that the highest labor endowment is 3 instead of 10. 	 
\end{description}

\subsection{Parametrization}
I parametrize the model according to table \ref{tab:KS_calibration} below.  The value for the tax rate, $\tau$, depends on the size of the unemployment benefit and the total labor income, hence it differs for the three economies.  

\begin{table}[H]
\begin{center}
\begin{tabular}{ clrrrr } 
\toprule
\textbf{Parameter} & \textbf{Description} & \textbf{Value} 
	& \textbf{E1} & \textbf{E2} & \textbf{E3}\\ 
\midrule
$\beta$ & Discount factor & 0.96 &&& \\ 
$\sigma$ & Relative risk aversion & 1.00 &&& \\ 
$\alpha$ & Capital share & $0.34$ &&& \\ 
$\underline{a}$ & Borrowing limit & 0 &&& \\ 
\midrule
$\tau$ & Tax rate && $0.34\%$ & $0.96\%$ & $1.21\%$\\ 
\bottomrule
\end{tabular}
\end{center}
\caption {Calibration parameters} 
\label{tab:KS_calibration} 
\end{table}


\subsection{Comparing the three economies}
The Gini coefficients for wealth and earnings (post taxes and transfers) for the three economies are reported in table \ref{tab:ginis}. The corresponding Lorenz curves are shown in figure \eqref{fig:lorenz_curves}. The figures for wealth, income (labor income and capital income) and consumption per wealth quintile are shown in table \ref{tab:quintiles}. 

% Table for Gini coefficients
\begin{center}
\setlength{\tabcolsep}{6pt}
\begin{table}[H]\centering
\ra{1.3}
\begin{tabular}{@{}rrr@{}}\toprule
& \textbf{Wealth} & \textbf{Earnings} \\ 
\midrule
%$dir=1$\\
$E1$ & 0.534 & 0.313 \\
$E2$ & 0.544 & 0.305 \\
$E3$ & 0.426 & 0.141 \\
\bottomrule
\end{tabular}
\caption{Gini coefficients. Earnings is defined as labor income, net of taxes, plus government transfers (unemployment benefits). } \label{tab:ginis}
\end{table}
\end{center}

Comparing economy E1 to economy E2, we see that by introducing the higher unemployment benefit the inequality in earnings (labor income post taxes and transfers) obviously decreases, but at the same time, wealth inequality increases. The reason is that the higher unemployment benefit serves as a buffer against the otherwise low consumption of an unemployed agent, therefore agents with little assets do not need to accumulate assets in the same amount as in economy E1. Looking at table \ref{tab:quintiles}, we see that the wealth held by the lowest wealth quintile drops by more than half, even though their consumption remains approximately the same. The consumption figures for all wealth quintiles are remarkably similar comparing E1 and E2, even though it is slightly higher in E1. In the absence of unemployment benefits the agents save themselves, however, when introducing the unemployment benefit, there is not as much need for private savings. 

Turning to economy E3, when the highest labor endowment is 3 instead of 10, the earnings inequality obviously decreases even more, and so does the wealth inequality. Since the agents with the highest labor endowment do not receive as much as before, in relative terms, the incentive to save decreases, and we see from table \ref{tab:quintiles} that the savings for the fifth quintile go down substantially. The aggregate capital stock shrinks, and total production goes down due to both less capital and less labor. 

To summarize, economy E1 gives the highest consumption for all wealth quintiles. In the absence of high unemployment benefits, the agents save more, which leads to a slightly higher capital stock, which in turns gives higher total production. Hence, lower benefits, higher income inequality and just slightly lower wealth inequality (compared to E2) can still give higher welfare for everyone. 

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/KS_Lorenz_sigma1_ex1.pdf}
                \caption{Economy 1}
                \label{fig:lorenz_E1}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/KS_Lorenz_sigma1_ex2.pdf}
                \caption{Economy 2}
        \end{subfigure}
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/KS_Lorenz_sigma1_ex3.pdf}
                \caption{Economy 3}
        \end{subfigure}
        \caption{Lorenz curves for the three economies}\label{fig:lorenz_curves}
\end{figure}

% Table for wealth quintiles
\begin{center}
\setlength{\tabcolsep}{4pt}
\begin{table}[H]\centering
\ra{1.3}
\begin{footnotesize}
\begin{tabular}{@{}rrrrrrcrrrrrcrrrrr@{}}\toprule
& \multicolumn{5}{c}{\textbf{Wealth}} & \phantom{}& \multicolumn{5}{c}{\textbf{Income}} &
\phantom{} & \multicolumn{5}{c}{\textbf{Consumption}}\\ 
\cmidrule{2-6} \cmidrule{8-12} \cmidrule{14-18}
& $1^{st}$ & $2^{nd}$ & $3^{rd}$ & $4^{th}$ & $5^{th}$ && 
$1^{st}$ & $2^{nd}$ & $3^{rd}$ & $4^{th}$ & $5^{th}$  && 
$1^{st}$ & $2^{nd}$ & $3^{rd}$ & $4^{th}$ & $5^{th}$ \\ 
\midrule
%$dir=1$\\
$E1$ & 0.29 & 2.77 & 7.11 & 13.22 & 26.79 && 
	   0.60 & 0.74 & 0.95 &  1.21 &  1.78 && 
	   0.53 & 0.73 & 0.96 &  1.24 &  1.82 \\
$E2$ & 0.11 & 2.61 & 7.04 & 13.23 & 26.89 && 
	   0.59 & 0.73 & 0.94 &  1.21 &  1.78 && 
	   0.52 & 0.72 & 0.95 &  1.23 &  1.82\\
$E3$ & 0.98 & 3.31 & 5.91 &  9.23 & 15.87 && 
	   0.53 & 0.65 & 0.76 &  0.90 &  1.18 && 
	   0.52 & 0.65 & 0.76 &  0.90 &  1.18\\ 
\bottomrule
\end{tabular}
\end{footnotesize}
\caption{Wealth, income and consumption per wealth quintile.} \label{tab:quintiles}
\end{table}
\end{center}

\subsection{Notes on implementation}
In this section, the algorithm used is briefly presented (since it follows very closely the algorithm discussed in class), and a few comments on the technical implementation are given. 
\subsubsection{Algorithm used}
\begin{description}
	\item[Create grids:] For the model I use the following number of grid points: $n_A=6000$, $n_K=10$. The end points for the grid for aggregate capital are defined after solving the Aiyagari model for first the low TFP shock (constant bust), and thereafter the high TFP shock (constant boom). The grid is thereafter defined as $K = [0.8\tilde{K}_b, 1.2 \tilde{K}_g]$ where $b$ indicates constant bust, and $g$ indicates constant boom from the Aiyagari model. 
	\item[Guess coefficients:] My initial guess of coefficients is simply $\gamma_0(A)=0$, $\gamma_1(A)=1 ~ \forall A$. 
	\item[Solve household problem:] I solve the household problem by VFI. This gives me resulting policy rules for next-period savings, $a'=g(a,l;A,K)$. 
	\item[Simulate:] Thereafter the economy is simulated 12,000 times, of which the first 2,000 are discarded as burn in periods.\footnote{The sequence of 12,000 TFP shocks is the same for all simulation rounds. I do have a problem that the distribution of the different states do not correspond perfectly to the invariant distribution. I guess two alternatives to fix this could be either to just have an even longer series of simulations, or to draw many different simulation series and then pick the one which best corresponds to the invariant distribution. } I start with an initial distribution $\Omega_0$ which is the resulting distribution from the Aiyagari model in the good state, and the corresponding aggregate capital. In each simulation, I do the following: 
\begin{itemize}
	\item Given the TFP shock and the aggregate capital, I use the policy function to find the next period's distribution. Since the aggregate capital most likely is somewhere in between two grid points of the $k$ grid, I have to interpolate the value functions. 
	\item Given the new distribution, I calculate the aggregate capital for the next period. 
	\item Then I draw the TFP shock for the next period. 
\end{itemize}
	\item[Regress:] The results from the simulations are partitioned into three groups, one for each level of TFP shock. Then I regress to find the new coefficients for the law of motion for capital. 
	\item[Update coefficients:] The coefficients are updated as a convex combination of previous guess and current updated values. Thereafter I go back and solve the household problem again. 
	\item[Calculate quintiles:] When the model has converged (after approximately 16 iterations for tolerance level of $10^{-6}$) I simulate 12,000 last rounds, this time also calculating wealth, income and consumption per wealth quintile in each simulation. The reason I am doing this only the last round is to save time (the time needed for the simulations more than doubles, from 15 sec to 35 sec). The reason why I don't store the complete information (such as income per $(a,l)$ tuple) is to save memory. 
\end{description}

\subsubsection{Technical implementation} 
The model is coded in Python, using numba (a method to just-in-time compile Python) for some time consuming computing tasks. This makes the model reasonably fast, it takes approximately 15 minutes to solve. 

\pagebreak
\appendix

\section{Huggett extra material}

\subsection{Value functions and policy functions from the autarky and fiat money economies}

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{../plots/huggett/huggett_aut_vfun_sigma1.pdf}
                \caption{Economy 1 - Autarky}
                \label{fig:huggett_aut_vfun}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{../plots/huggett/huggett_meq_vfun_sigma1.pdf}
                \caption{Economy 2 - Money equilibrium}
        \end{subfigure}
        \caption{Value functions for the two economies}\label{fig:huggett_vfun}
\end{figure}

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{../plots/huggett/huggett_aut_pfun_sigma1.pdf}
                \caption{Economy 1 - Autarky}
                \label{fig:huggett_aut_pfun}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{../plots/huggett/huggett_meq_pfun_sigma1.pdf}
                \caption{Economy 2 - Money equilibrium}
        \end{subfigure}
        \caption{Policy functions for the two economies}\label{fig:huggett_pfun}
\end{figure}


\section{Krusell and Smith extra material}

\subsection{Value functions, policy functions and resulting distributions from the Aiyagari model}

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_vfun_sigma1_ex1_tfp1.pdf}
                \caption{Economy 1}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_vfun_sigma1_ex2_tfp1.pdf}
                \caption{Economy 2}
        \end{subfigure}
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_vfun_sigma1_ex3_tfp1.pdf}
                \caption{Economy 3}
        \end{subfigure}
        \caption{Value functions for the three economies (TFP=1.0)}\label{fig:Aiy_vfun}
\end{figure}

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_pfun_sigma1_ex1_tfp1.pdf}
                \caption{Economy 1}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_pfun_sigma1_ex2_tfp1.pdf}
                \caption{Economy 2}
        \end{subfigure}
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_pfun_sigma1_ex3_tfp1.pdf}
                \caption{Economy 3}
        \end{subfigure}
        \caption{Policy functions for the three economies (TFP=1.0)}\label{fig:Aiy_pfun}
\end{figure}

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_dist_sigma1_ex1_tfp1.pdf}
                \caption{Economy 1}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_dist_sigma1_ex2_tfp1.pdf}
                \caption{Economy 2}
        \end{subfigure}
        \begin{subfigure}[b]{0.33\textwidth}
                \includegraphics[width=\textwidth]{../plots/ks/Aiy_dist_sigma1_ex3_tfp1.pdf}
                \caption{Economy 3}
        \end{subfigure}
        \caption{Distributions for the three economies (TFP=1.0)}\label{fig:Aiy_dist}
\end{figure}

\pagebreak
\subsection{Equilibrium coefficients from the KS model}
Solving for the equilibrium in the three economies we get the following coefficients for the law of motion of aggregate capital:
% Table for coefficients 
\begin{center}
\setlength{\tabcolsep}{4pt}
\begin{table}[H]\centering
\ra{1.3}
\begin{tabular}{@{}rrrcrrcrr@{}}\toprule
& \multicolumn{2}{c}{$\textbf{A=0.85}$} & \phantom{}& \multicolumn{2}{c}{$\textbf{A=1.0}$} & \phantom{} & \multicolumn{2}{c}{$\textbf{A=1.05}$}\\ 
\cmidrule{2-3} \cmidrule{5-6} \cmidrule{8-9}
& $\gamma_0$ & $\gamma_b$ && $\gamma_0$ & $\gamma_1$ && $\gamma_0$ & $\gamma_1$ \\ 
\midrule
%$dir=1$\\
$E1$ & 0.114 & 0.967 && 0.144 & 0.963 && 0.152 & 0.962 \\
$E2$ & 0.110 & 0.968 && 0.141 & 0.964 && 0.150 & 0.962 \\
$E3$ & 0.101 & 0.967 && 0.134 & 0.962 && 0.141 & 0.961 \\
\bottomrule
\end{tabular}
\caption{Coefficients for the law of motion of capital.} \label{tab:coefficients}
\end{table}
\end{center}


\end{document}