\documentclass{scrartcl}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage{float}
%\usepackage{undertilde}
%\usepackage{mathrsfs}
\usepackage{epstopdf}
\usepackage{listings}
\usepackage{tabu}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{booktabs,caption,fixltx2e}
\usepackage[flushleft]{threeparttable}
%%\usepackage{courier}
\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
\usepackage{booktabs}
\usepackage{fancyhdr}
\usepackage[colorlinks=true, hidelinks]{hyperref}
\usepackage[tmargin=1.1in,bmargin=1.1in,lmargin=1.45in,rmargin=1.45in]{geometry}

%% User defined commands
\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
	% Integrals
\newcommand*\diff{\mathop{}\!\mathrm{d}}
	% Symbols
\newcommand{\RR}{\mathbb{R}}
\newcommand{\LL}{\mathscr{L}}
	% Operators
\newcommand{\Max}[2]{\max_{#1} \left\{ #2 \right\} } % Maximum
\newcommand{\Min}[2]{\min_{#1} \left\{ #2 \right\} } % Minimum
\newcommand{\Argmax}[2]{\argmax_{#1} \left\{ #2 \right\} }
\newcommand{\Argmin}[2]{\argmin_{#1} \left\{ #2 \right\} }
\newcommand{\Ln}[1]{\ln{ \left( #1 \right)} }
\newcommand{\Sumt}{\sum_{t=0}^\infty }
	% Statistics
\newcommand{\E}[1]{\text{E} \left[ #1 \right]}
\newcommand{\Var}[1]{\text{Var} \left( #1 \right)}
\newcommand{\Cov}[1]{\text{Cov} \left( #1 \right)}
\newcommand{\Y}{\utilde{Y}}
\newcommand{\X}{\utilde{X}}
\newcommand{\Z}{\utilde{Z}}
\newcommand{\e}{\utilde{e}}
\newcommand{\ML}[1]{\hat{1}_{\text{ML}}}
	% Parantheses
		% Normal
\newcommand{\Le}{\left(}
\newcommand{\Ri}{\right)}
\newcommand{\Lep}{\left(}
\newcommand{\Rip}{\right)}
\newcommand{\lep}{\left(}
\newcommand{\rip}{\right)}
		% Curly
\newcommand{\Lec}{\left\{}
\newcommand{\Ric}{\right\}}
\newcommand{\lec}{\left\{}
\newcommand{\ric}{\right\}}
		% Hard
\newcommand{\Leh}{\left[}
\newcommand{\Rih}{\right]}
\newcommand{\leh}{\left[}
\newcommand{\rih}{\right]}

\definecolor{steelblue}{RGB}{64,134,170}

\begin{document}

\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE,RO]{\thepage}
\lhead{Quantitative Macro Methods 2015, Jonna Olsson}
\fancyfoot{}

\title{Quantitative Macro Methods \\ Research Projects}
\subtitle{Problem Set 2}
\author{Jonna Olsson\thanks{jonna.olsson@ne.su.se}}
\maketitle

\section*{Introduction}
In this document the findings from the research project are documented. The algorithms used to solve the models are briefly described, all code used for the project can be found at the following web page: \url{https://bitbucket.org/jonnaolsson/quant2015/overview}. Hopefully it should be clear where you can find what in the code. The code can also be downloaded fairly easily if you want to run anything, just let me know if there is any problem. 

\section*{Question 1}
I solve the model with value function iteration, policy function iteration and endogenous grid point method. Note that I for the policy function iteration use grid search. The resulting policy functions for consumption are shown for different model ages in figure \ref{fig:comp_cf_vfi_pfi_egm}. The EGM gives as expected smoother policy functions, while the grid search methods give the characteristic sawtooth shapes. At the age of 45 and upwards, where there is no future uncertainty\footnote{Note that I interpret the question as if we work for the first 45 years of the life, and that we have uncertainty about our wage also in the last year of working. This seems to better match the age profile that we are given, where age 46 is where the age profile drops to 0.68. Sorry if this is not what was intended. Changing this is easy and does not make any qualitative difference anywhere.}, the VFI and the PFI follows the EGM line almost perfectly (except for the sawtooth shape), while at younger ages we have a tendency of the EGM being slightly below the VFI and the PFI for high levels of cash-on-hand. The main reason is that I do not allow for extrapolation in the VFI and the PFI. However, up to the levels of normalized cash-on-hand that are actually encountered in a simulation, the policy functions from the three separate methods are indistinguishable. 

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_cfun_sigma2_ex1_age10_nyy9.pdf}
                \caption{Model age 10}
                \label{fig:huggett_aut_vfun}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_cfun_sigma2_ex1_age35_nyy9.pdf}
                \caption{Model age 35}
        \end{subfigure}
		\begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_cfun_sigma2_ex1_age45_nyy9.pdf}
                \caption{Model age 45}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_cfun_sigma2_ex1_age55_nyy9.pdf}
                \caption{Model age 55}
        \end{subfigure}
        \caption{Consumption functions for different model ages} \label{fig:comp_cf_vfi_pfi_egm}
\end{figure}

Since the policy functions are the same, the resulting age profiles are also the same. The simulated life cycle profiles (averaged over a cross section of 100,000 individuals) from the three different solution methods can be found in in figure \ref{fig:comp_aprof_vfi_pfi_egm} in the appendix.

In this specific question, I would choose the EGM, since the problem is suitable for that solving method (there are for instance no discrete choices as in question 5). EGM is in this case more precise and also the fastest method. If increasing the precision is important, another solution could be to solve it with PFI, but not via grid search but root finding. 

If I would like to use VFI, but increase the precision, I should increase the number of grid points even more (now the problem is solved with $nss=1,600$ and $nxx=2,000$). However, then putting some more effort into speeding up the code by exploiting for instance the monotonicity and concavity of the problem could be helpful. 


\section*{Question 2}
If I vary the number of quadrature nodes from 9 (the default value I am using) to anything else that is above 1, the results are identical. Only $n$ nodes are needed to perfectly approximate a polynomial of degree $2n+1$, which is sufficient in this case. In this problem we do not encounter any other problematic features either (see further discussion in question 3). However, of course if I use only one node, the results become wrong. For some illustrations, see figure \ref{fig:comp_qn_question2}. 

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_cfun_sigma2_ex1_age10_nyy1.pdf}
                \caption{Quadrature nodes: 1}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_ageprof_sigma2_ex1_method_VFI_nyy1.pdf}
                \caption{Quadrature nodes: 1}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_cfun_sigma2_ex1_age10_nyy2.pdf}
                \caption{Quadrature nodes: 2}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_ageprof_sigma2_ex1_method_VFI_nyy2.pdf}
                \caption{Quadrature nodes: 2}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_cfun_sigma2_ex1_age10_nyy9.pdf}
                \caption{Quadrature nodes: 9}
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_ageprof_sigma2_ex1_method_VFI_nyy9.pdf}
                \caption{Quadrature nodes: 9}
        \end{subfigure}
        \caption{Varying the number of quadrature nodes} \label{fig:comp_qn_question2}
\end{figure}

\section*{Question 3}
Now we introduce borrowing, we assume that households can borrow up to the level of their permanent income: 
\[
S_t \geq -P_t
\]
In practice this means that their normalized savings cannot be smaller than $-1$. 

\subsection*{Subquestion (a)}
The model is solved with value function iteration, using 9 quadrature nodes. The solution algorithm from the previous exercise is adapted in the following way:
\begin{itemize}
	\item The minimum grid point for normalized savings is changed to $-1$, and the cash-on-hand grid is also changed accordingly.
	\item When initializing the value function for the last period, I have to handle the fact that consumption cannot be negative. I initialize the value function to an extremely large negative number for all states where cash-on-hand is negative, i.e. where consumption would be negative.
	\item When I compute next-period expected value, I check if next-period cash-on-hand will imply negative consumption (i.e. if it is smaller than what we maximum can borrow). If so, the resulting value in the value function is set to an extremely large negative number. 
\end{itemize}
 
\subsection*{Subquestion (b)}
When comparing the no-borrowing case and the case when households can borrow, the resulting policy functions for savings differ at low levels of cash-on-hand, as can be seen in figure \ref{fig:comp_sfun_question3}. When households are not allowed to borrow, they do not save anything for low values of cash-on-hand, but consume everything, as can be seen in panel (a). They would have liked to borrow if they had had the option. 

When households are allowed to borrow, they do so for low levels of cash-on-hand. For old ages, when there is no uncertainty, households with negative cash-on-hand choose to borrow maximum due to impatience. 

However, when households are younger and there is uncertainty in their future income, they display a different behavior for positions close to the borrowing limit. Below a certain value, the precautionary motive dominates, and the households do not borrow maximum, as they would have done if they were older and had no uncertainty. 

The resulting changes in the simulated life cycle profiles are shown in figure \ref{fig:comp_aprof_question3}. As expected, the savings are lower in the case when borrowing is allowed. The households borrow as young in order to front-load consumption, which can be seen from the higher consumption level in very young ages when borrowing is allowed. 

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_sfun_sigma2_ex1_method_VFI_nyy9.pdf}
                \caption{Ex.1, without borrowing}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_sfun_sigma2_ex3_method_VFI_nyy9.pdf}
                \caption{Ex.3, with borrowing}
        \end{subfigure}
        \caption{Policy functions for savings without and with borrowing}
 \label{fig:comp_sfun_question3}
\end{figure}

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=0.8\textwidth]{../plots/lc/LC_compare_sigma2_ex1_vs_ex3_nyy9.pdf} 
	\caption{Simulated life cycle profiles for exercise 1, without borrowing in solid, and exercise 3, with borrowing, in dashed lines.}
	\label{fig:comp_aprof_question3}
	\end{center}
\end{figure}

\subsection*{Subquestion (c)}
The effect of varying the number of quadrature nodes can be seen in figure \ref{fig:comp_qn_question3}. The fewer the quadrature nodes,the more the households borrow. The reason is how the quadrature node structure works in combination with the algorithm for negative consumption. In the algorithm, I assign all states where consumption would be negative a very large negative number. When I increase the number of quadrature nodes, I also increase the number of times when this unfortunate state is reached. Hence, if I would have an infinite number of quadrature nodes, and set the very large negative number to $-\infty$, the result would be equivalent to impose a natural borrowing constraint (i.e. $S_t \leq 0$, since the realization of income can be arbitrarily close to zero -- both $N$ and $V$ are log-normally distributed). 

The root of the problem is that I in the original implementation have an (albeit very small) probability of households defaulting. As soon as they start to borrow, they might end up in default, since their lowest possible income realization is arbitrarily close to 0. The problem is that I do not handle this explicitly in the algorithm. 

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.6\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_compare_sigma2_ex1_vs_ex3_nyy3.pdf}
                \caption{Quadrature nodes: 3}
        \end{subfigure} 
        \begin{subfigure}[b]{0.6\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_compare_sigma2_ex1_vs_ex3_nyy9.pdf}
                \caption{Quadrature nodes: 9}
        \end{subfigure}
		\begin{subfigure}[b]{0.6\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_compare_sigma2_ex1_vs_ex3_nyy11.pdf}
                \caption{Quadrature nodes: 11}
        \end{subfigure}
        \caption{Age profiles for different numbers of quadrature nodes}\label{fig:comp_qn_question3}
\end{figure}

\subsection*{Subquestion (d)}
If I would implement this problem with EGM, the main difficulty would be to take care of the states when the future consumption would be negative. If I e.g. set the consumption to zero for those states, the right hand side of the Euler Equation would be infinitely large, and hence minimizing the EE error would not work for those cases. 

\section*{Question 4}
Now assume that households can borrow as in previous question, but the interest rate for borrowing is higher than the interest rate for saving. 

\subsection*{Subquestion (a)}
The model is solved with value function iteration, using 9 quadrature nodes. The solution algorithm from the previous exercise is adapted in the following way:
\begin{itemize}
	\item In the value function iteration, I check if the savings level is below 0 or not. If it is below zero, the the higher interest rate is used, otherwise the lower. 
	\item Same check is performed in the simulation. 
\end{itemize}

\subsection*{Subquestion (b)}
The policy functions for savings differ very little compared to previous exercise, as can be seen in figure \ref{fig:comp_sfun_question4}. However, looking at them closer shows some very small differences -- households save slightly less when faced with the higher interest rate for borrowing. 

Looking at the resulting simulated life cycle profiles, the differences are discernible, see figure \ref{fig:comp_aprof_question4}. If faced with a higher interest rate when borrowing, households borrow less, as expected. 

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_sfun_sigma2_ex3_method_VFI_nyy9.pdf}
                \caption{Ex.3, same interest rates}
        \end{subfigure}% ~ %or \quad, \qquad, \hfill etc.
        \begin{subfigure}[b]{0.49\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_sfun_sigma2_ex4_method_VFI_nyy9.pdf}
                \caption{Ex.4, higher interest rate when borrowing}
        \end{subfigure}
        \caption{Policy functions for savings for exercise 3 (same interest rate for saving and borrowing) and exercise 4 (higher interest rate for borrowing). }
 \label{fig:comp_sfun_question4}
\end{figure}

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=0.8\textwidth]{../plots/lc/LC_compare_sigma2_ex3_vs_ex4_nyy9.pdf} 
	\caption{Comparing simulated life cycle profiles for exercise 3 (same interest rate for saving and borrowing) and exercise 4 (higher interest rate for borrowing).}
	\label{fig:comp_aprof_question4}
	\end{center}
\end{figure}

\section*{Question 5}
Now assume that households have the opportunity to default. If they choose to default, they don't have to repay their debt (but they keep their current income) and they will be excluded forever from borrowing. 

\subsection*{Subquestion (a)}
The model is solved with value function iteration, using 9 quadrature nodes. To solve the model I need two value functions: one for non-defaulting and one for defaulting. The value function for non-defaulting must take into account the value of the option to default at any future point in time. Hence, I do the following: 
\begin{itemize}
	\item The value function for defaulting I already have, this is the one solved for in exercise 1. 
	\item Iterating over the value function for non-defaulting, in every node when I calculate the expected value of tomorrow, I compare for that particular realization the value of tomorrow of non-defaulting (as a function of expected cash-on-hand) with the value of defaulting (as a function of expected income), and pick as continuation value the max of these two. 
	\item Now I have two value functions -- one for non-defaulting, and one for defaulting. 
	\item I also have two policy functions -- one for non-defaulting, and one for defaulting. 
	\item In the simulation, I then check which households in my panel would like to default, by simply comparing the value for non-defaulting (as a function of their cash-on-hand) and the value of defaulting (as a function of their current income). 
	\item For those households which have not defaulted previously, whichever value is largest decide their action. 
	\item The newly defaulted have their financial assets set to zero (but they keep their current income). 
	\item For those households which have defaulted, (in this period or earlier) choose optimal savings according to the policy function for the defaulted. 
	\item The households which still not have defaulted choose optimal savings (positive or negative) according to the policy function for the non-defaulting case. 
\end{itemize}

\subsection*{Subquestion (b)}
The default rules are quite generous in this case -- the households can actually just borrow, and then get their debt reset to zero at some point, without any cost (except that they are not allowed to take up new debt). Considering what we have learned from previous exercises, households want to borrow early in life, to smooth their consumption in early years with low income, and thereafter start saving for retirement. Hence, it is not surprising that every household will use this default option at some point in their life. Around the age of 35 (model years 15) all households have used the option to reset their financial debt to zero. What they do is that when they previously started to repay their debt before starting saving for retirement, they now choose to default instead. 

As can be seen from the simulated life-cycle profiles in figure \ref{fig:comp_aprof_question5}, households take on more debt in the beginning when they know they have the option to default. Thereafter, households start defaulting, and the average savings quickly rises, up to almost the same level as when there was no borrowing (see figure \ref{fig:comp_aprof_question5(1)} for a comparison with exercise 1 where we had no borrowing). 

Compared to exercise 4, when there was no default option, the households are better off when they can default. For the first years, the difference is more pronounced (first year, the average increase in consumption is 13\%). After a while the effect has almost worn off, but still the increase in consumption is still around one half of a percent until the end of life, see figure \ref{fig:comp_cons_question5}. 



\begin{figure}[H]
	\begin{center}
	\includegraphics[width=0.8\textwidth]{../plots/lc/LC_default_sigma2_ex5.pdf} 
	\caption{Fraction of households which have defaulted as a function of age.}
	\label{fig:default_ratios}
	\end{center}
\end{figure}


\begin{figure}[H]
	\begin{center}
	\includegraphics[width=0.8\textwidth]{../plots/lc/LC_compare_sigma2_ex4_vs_ex5_nyy9.pdf} 
	\caption{Comparing simulated life cycle profiles for exercise 4 (no default option) and exercise 5 (default option).}
	\label{fig:comp_aprof_question5}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=0.8\textwidth]{../plots/lc/LC_cons_inc_sigma2_ex5.pdf} 
	\caption{Increase in consumption (in percent) comparing exercise 4 (no default option, lower consumption) and exercise 5 (default option, higher consumption).}
	\label{fig:comp_cons_question5}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
	\includegraphics[width=0.8\textwidth]{../plots/lc/LC_compare_sigma2_ex1_vs_ex5_nyy9.pdf} 
	\caption{Comparing simulated life cycle profiles for exercise 1 (no borrowing) and exercise 5 (borrowing with default option).}
	\label{fig:comp_aprof_question5(1)}
	\end{center}
\end{figure}




\pagebreak
\appendix

\section{Question 1}

\subsection{Age profiles for different solution methods}

\begin{figure}[H]
        \centering
        \begin{subfigure}[b]{0.55\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_ageprof_sigma2_ex1_method_VFI_nyy9.pdf}
                \caption{Value function iteration}
        \end{subfigure} 
        \begin{subfigure}[b]{0.55\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_ageprof_sigma2_ex1_method_PFI_nyy9.pdf}
                \caption{Policy function iteration}
        \end{subfigure}
		\begin{subfigure}[b]{0.55\textwidth}
                \includegraphics[width=\textwidth]{../plots/lc/LC_ageprof_sigma2_ex1_method_EGM_nyy9.pdf}
                \caption{Endogenous grid point method}
        \end{subfigure}
        \caption{Age profiles for the three solution methods}\label{fig:comp_aprof_vfi_pfi_egm}
\end{figure}



\end{document}